"use strict";

var appServices    = angular.module('services', []),
  appControllers = angular.module('controllers', []);

angular.module('app', ['ionic', 'controllers', 'routes', 'services',
  'ngCordova','ionic.contrib.drawer', 'ngMaterial', 'ngMessages'])

  .run(function($ionicPlatform, $rootScope) {

    $rootScope.DEBUG_MODE = DEBUG_MODE;

    $rootScope.PLATFORM = ionic.Platform;

    $rootScope.MODULES = APP_MODULES;

    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }

      $ionicPlatform.registerBackButtonAction(function () {
        //Do nothing
      }, 100);

      //checkForUpdates();

    });//platform.ready?

    var checkForUpdates = function(){
      if($rootScope.MODULES.UPDATES && $rootScope.PLATFORM.isWebView()) {
        var deploy = new Ionic.Deploy();
        deploy.watch().then(function () {}, function () {}, function (hasUpdate) {
          console.log('Buscando actualizaciones...');
          if (hasUpdate && !$rootScope.showed) {
            $rootScope.showed = true;
            $ionicPopup.confirm({
              title: "Actualizaciones",
              content: "Hay una nueva actualización, ¿Desea instalarla ahora?",
              cancelType: "button-default",
              cancelText: "No",
              okType: "button-positive",
              okText: "Sí"
            }).then(function (resp) {
              if (resp) {
                $ionicLoading.show({
                  template: "Actualizando...",
                  //content: "Actualizando 2",
                  animation: 'fade-in',
                  showBackdrop: true,
                  maxWidth: 200,
                  showDelay: 0
                });
                deploy.update().then(function (res) {
                  console.log('Ionic Deploy: Update Success! ', res);
                }, function (err) {
                  console.log('Ionic Deploy: Update error! ', err);
                }, function (prog) {
                  console.log('Ionic Deploy: Progress... ', prog);
                });
              }
            });
          }
        });
      }
    }
  });

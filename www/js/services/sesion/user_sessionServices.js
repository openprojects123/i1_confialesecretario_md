"use strict";
/**
 * Servicio que extiende la funcionalidad de: sessionSvc
 */
appServices.factory("userSvc", function($rootScope, $q, sessionSvc, utils){
  var USER = {
    nombre:    "",
    paterno:   "",
    materno:   "",
    email:     "",
    telefono:  "",
    buttonMAC: [],
    isRegistered: false,
    isValidCode:  false
  };

  var _u = localStorage.getItem("USER");
  if(_u)
    USER = JSON.parse(_u);

  /**
   * @description
   * Almacena el objeto USER en localstorage
   */
  function persistUser() {
    localStorage.setItem("USER", JSON.stringify(USER));
  }

  /**
   * @description
   * Obtiene una "copia" del Usuario
   * @returns USER object
   */
  function getUser(){
    return USER;
  }

  /**
   * @description
   * Extiende la funcionalidad de sessionSvc.register()
   */
  function register(form) {
    var deferred = $q.defer();
    //utils.putMsg.log(form);
    sessionSvc.register(form)
      .then(function (regResp) {
        USER.isRegistered = true;
        USER.isValidCode  = true;

        persistUser();

        deferred.resolve(regResp);

      }, function (error) {
        deferred.reject(error);
      });
    return deferred.promise;
  }

  function validateCode(form) {
    var deferred = $q.defer();

    sessionSvc.validateCode(form)
    .then(function (apiData) {
      USER.isRegistered = true;
      USER.isValidCode  = true;
      USER.nombre       = apiData.nombre;
      USER.paterno      = apiData.paterno;
      USER.materno      = apiData.materno;
      USER.email        = apiData.email;
      USER.telefono     = apiData.telefono;
      USER.buttonMAC    = [];//apiData.macBtn;

      persistUser();

      deferred.resolve(USER);
    }, function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  /**
   * @description
   * Extiende la funcionalidad de sessionSvc.login()
   * Seteando los atributos del usuario
   */
  function login(form) {
    var deferred = $q.defer();

    sessionSvc.login(form)
    .then(function (apiData) {//object | false

      /*/fixme*******************************************************************
      //*                             PARA DEBUG                               *
      //************************************************************************
      utils.putMsg.error("**** user_sessionServices.login() en modo DEBUG  ****");
      //Atributos personalizados para esta App
      USER.nombre    = "*nombre*";
      USER.paterno   = "*paterno*";
      USER.materno   = "*materno*";
      USER.email     = "*email*";
      USER.telefono  = "1000000009";
      USER.buttonMAC = "*MAC-123*";
      //******************************FIN DEBUG **********************************/


      USER.isRegistered = true;
      USER.isValidCode  = true;
      USER.nombre       = apiData.nombre;
      USER.paterno      = apiData.paterno;
      USER.materno      = apiData.materno;
      USER.email        = apiData.email;
      USER.telefono     = apiData.telefono;
      USER.buttonMAC    = [];//apiData.macBtn;

      persistUser();

      deferred.resolve(USER);

    }, function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  /**
   * @description
   * Extiende la funcionalidad de sessionSvc.logout()
   * Cierra la sesion y resetea los atributos de USER
   */
  function logout() {
    sessionSvc.logout();
    localStorage.removeItem("USER");

    //seteo los atributos personalizados por defecto
    USER = {}
  }

  /**
   * @description
   * Extiende la funcionalidad de sessionSvc.changePassword()
   */
  function changePassword(form) {
    return sessionSvc.changePassword(form);
  }

  /**
   * @description
   * Extiende la funcionalidad de sessionSvc.recoveryPassword()
   */
  function recoveryPassword(form) {
    return sessionSvc.recoveryPassword(form);
  }

  return{
    getUser:          getUser,
    register:         register,
    validateCode:     validateCode,
    login:            login,
    logout:           logout,
    changePassword:   changePassword,
    recoveryPassword: recoveryPassword
  }
});

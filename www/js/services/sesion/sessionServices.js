"use strict";

/**
 * Servicio base para el manejo de sesion de usuario.
 * Se debe extender la funcionalidad en un servicio adicional
 * (i.e. userCustomTypeSvc).
 */
appServices.factory("sessionSvc", function($rootScope, $q, utils){

  //Sesion base
  var SESSION = {
    userId:          null,
    userToken:       null,
    isAuthenticated: !APP_MODULES.SESSION.USER_AUTH,
    isRemembered:    false
  };

  function cloneSession() {
    return JSON.parse(JSON.stringify(SESSION));
  }

  function persistSession() {
    localStorage.setItem("SESSION", JSON.stringify(SESSION));
  }

  var _s = localStorage.getItem("SESSION");
  var _SESSION;

  if(_s){
    _SESSION = JSON.parse(_s);
    /*SESSION  = JSON.parse(_s);*/
  }
  else
    _SESSION = cloneSession();


  $rootScope.SESSION_ATTRS = {
    getUserId:        function () {return _SESSION.userId},
    getUserToken:     function () {return _SESSION.userToken},
    isAuthenticated:  function () {return _SESSION.isAuthenticated},
    isRemembered:     function () {return _SESSION.isRemembered},
  };

  /**
   * @description
   * Método para registrar a un usuario
   * @param form (Object) {}
   *   Debe contener los campos: {_username, _password}
   * @returns {Promise}
   * success:
   *   {result: (true|false), data: {_userId, codigo} }
   * fail:
   *   (Object) parsedError
   */
  function register(form) {
    var deferred = $q.defer();

    /*/fixme*******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
    utils.putMsg.error("**** sessionServices.register() en modo DEBUG  ****");
    utils.putMsg.log(form);
    utils.timeout(function () {
      //utils.showAlertDialog("userSvc.register() en modo DEBUG");
      var resp = {result: true, data:{_userId:123, _username: "Debug "+form._username}};
      utils.putMsg.success("sessionServices.register() en modo DEBUG completada");
      deferred.resolve(resp.data);
    }, 5*ONE_SECOND);
    return deferred.promise;
    //*************** FIN DEBUG ***************/

    var form2 = JSON.parse(JSON.stringify(form));
    form2.password1 = form2._password = CryptoJS.MD5(form._password).toString();

    utils.myHttpPost(
      API_URL + "public/usuario/new",
      form2
    ).then(function(API){
      if(API.result){
        SESSION.userId = API.data._userId;
        deferred.resolve(API.data);
      }
      else{
        deferred.reject(utils.getError(API, "register(API)"));
      }
    }, function (error) {
      deferred.reject(error, "register()");
    });

    return deferred.promise;
  }

  /**
   * valida el codigo recibido para poder el proceso
   * de registro de usuario
   * @param codigo
   * @returns {*|promise}
   * success:
   *   {result: (true|false),
   *    data: {_userId, token, nombre, paterno, ...},//Solo cuando result==true
   *    error_message(string) }, //Solo cuando result == false
   * fail:
   *   (Object) parsedError
   */
  function validateCode(codigo) {
    var deferred = $q.defer();

    utils.myHttpGet(API_URL+"public/usuario/checkCode/"+codigo)
    .then(function (API) {
      if(API.result){
        SESSION.userId          = API.data._userId;//a pesar de haberlo seteado en el registro
        SESSION.userToken       = API.data.token;
        SESSION.isAuthenticated = true;

        persistSession();
        _SESSION = cloneSession();

        deferred.resolve(API.data);
      }
      else{
        deferred.reject(utils.getError(API, "validateCode(API)"));
      }
    }, function (error) {
      deferred.reject(error, "validateCode()");
    });

    return deferred.promise;
  }

  /**
   * @description
   * Metodo para autentificar un usuario
   * @param form (Object) {}
   *   Debe contener los campos: {_username, _password}
   * @returns {Promise}
   * success:
   *   {result: (true|false),
   *    data: {_userId, token, nombre, paterno, ...},//Solo cuando result==true
   *    error_message(string) }, //Solo cuando result == false
   * fail:
   *   (Object) parsedError
   */
  function login(form) {
    var deferred = $q.defer();

    /*/fixme*******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
    utils.putMsg.error("**** sessionServices.login() en modo DEBUG  ****");
    utils.timeout(function () {
      SESSION.userId          = 1;
      SESSION.isAuthenticated = true;
      SESSION.isRemembered    = form.isRemembered;

      persistSession();
      _SESSION = cloneSession();

      utils.putMsg.success("sessionServices.login() en modo DEBUG completada");
      deferred.resolve({result: true, data:{_userId:123, _username: "Debug "+form._username} });
    }, 5*ONE_SECOND);
    return deferred.promise;
    //******************************FIN DEBUG **********************************/

    var encPassword = CryptoJS.MD5(form._password).toString();
    utils.myHttpPost(
      API_URL + "public/usuario/login",
      {loginField: form.loginField, _password: encPassword}
    ).then(function(API){
      if(API.result){
        SESSION.userId          = API.data._userId;
        SESSION.userToken       = API.data.token;
        SESSION.isAuthenticated = true;
        SESSION.isRemembered    = form.isRemembered;

        persistSession();
        _SESSION = cloneSession();

        deferred.resolve(API.data);
      }
      else{
        deferred.reject(utils.getError(API, "login(API)"));
      }
    }, function (parsedError) {
      deferred.reject(parsedError, "login()");
    });

    return deferred.promise;
  }

  /**
   * @description
   * Metodo para actualizar el password de usuario
   * @param form (Object) {}
   *   Debe contener los campos: {_userId, _oldPassword, _newPassword}
   * @returns {Promise}
   * success:
   *   {result: (true|false)}
   * fail:
   *   (Object) parsedError
   */
  function changePassword(form) {
    var deferred = $q.defer();

    /*/fixme*******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
    utils.putMsg.error("**** sessionServices.changePassword() en modo DEBUG  ****");
    utils.putMsg.log(form);
    utils.timeout(function () {
      //utils.showAlertDialog("userSvc.register() en modo DEBUG");
      var resp = {result: true};
      utils.putMsg.success("sessionServices.changePassword() en modo DEBUG completada");
      deferred.resolve(resp.result);
    }, 5*ONE_SECOND);
    return deferred.promise;
    //*************** FIN DEBUG ***************/

    var
      oldPassword = CryptoJS.MD5(form._oldPassword).toString(),
      newPassword = CryptoJS.MD5(form._newPassword).toString();

    utils.myHttpPost(
      API_URL + "public/usuario/passChange",
      {
        _userId:      _SESSION.userId,
        _oldPassword: oldPassword,
        _newPassword: newPassword
      }
    ).then(function(API){
      if(API.result)
        deferred.resolve(true);
      else{
        deferred.reject(utils.getError(API, "changePassword(API)"));
      }
    }, function (parsedError) {
      deferred.reject(parsedError, "changePassword(API)");
    });

    return deferred.promise;
  }

  /**
   * @description
   * Metodo para recuperar el password de usuario, elbackend debe
   * enviar un email o SMS con el nuevo password
   * @param form (Object) {}
   *   Debe contener el campo: {_username}, puede ser nombre de usuario, email
   *   o lo que se usa como campo unico
   * @returns {Promise}
   * success:
   *   {result: (true|false), data:{message: (string)}}
   * fail:
   *   (Object) parsedError
   */
  function recoveryPassword(form) {
    var deferred = $q.defer();

    /*/fixme*******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
    utils.putMsg.error("**** sessionServices.recoveryPassword() en modo DEBUG  ****");
    utils.putMsg.log(form);
    utils.timeout(function () {
      var resp = {result: true, data:{instructions: "Se le ha enviado un email con..."}};
      utils.putMsg.success("sessionServices.recoveryPassword() en modo DEBUG completada");
      deferred.resolve(resp.data.instructions);
    }, 5*ONE_SECOND);
    return deferred.promise;
    //*************** FIN DEBUG ***************/

    utils.myHttpPost(
      API_URL + "public/usuario/passRecovery", {_username: form._username}
    ).then(function(API){
      if(API.result)
        deferred.resolve(API.data);
      else{
        deferred.reject(utils.getError(API, "recoveryPassword(API)"));
      }
    }, function (parsedError) {
      deferred.reject(parsedError, "recoveryPassword()");
    });

    return deferred.promise;
  }

  /**
   * @description
   * Reset de atributos de sesion.
   */
  function logout() {
    localStorage.removeItem("SESSION");
    SESSION  = {};
    _SESSION = {};
  }

  return {
    register:         register,
    validateCode:     validateCode,
    login:            login,
    logout:           logout,
    changePassword:   changePassword,
    recoveryPassword: recoveryPassword
  };

});

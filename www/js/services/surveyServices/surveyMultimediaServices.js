"use strict";

appServices.factory("surveyMultimediaSvc", function($q, fileMgrSvc, utils){
  /**
   * Actualiza el estatus de un ticket a 1, indicando que se ha
   * contestado la encuesta... supongo
   * @param idTicket
   * @param status = 1
   * @returns {Promise}
   */
  function updateTicketStatus(idTicket, status) {
    var deferred = $q.defer();

    utils.myHttpPut(
      API_URL + "common/ticket/"+idTicket+"/status/"+status
    ).then(function(API){
      if(API.result)
        deferred.resolve(true);
      else
        deferred.reject(utils.getError(API, "updateTicketStatus(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "updateTicketStatus()"));
    });

    return deferred.promise;
  }

  /**
   * Helper que unicamente ejecuta a fileMgrSvc.multiUpload()
   * @returns {Promise}
   */
  function uploadFileList() {
    var deferred = $q.defer();

    fileMgrSvc.multiUpload()
    .then(function (numUploadedOk) {
      deferred.resolve(numUploadedOk);
    }, function (err) {
      deferred.reject(err)
    });

    return deferred.promise;
  }

  /**
   * Helper que uicamente ejecuta a fileMgrSvc.removeFile(id)
   * @param id indice del archivo en el arreglo
   * @returns {Promise}
   */
  function removeFile(id) {
    var deferred = $q.defer();

    fileMgrSvc.removeFile(id)
    .then(function (resp) {
      deferred.resolve(resp);
    }, function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  /**
   * Almacena el comentario multimedia
   * @param formData
   * @returns {Promise}
   */
  function saveComentaTicket(idTicket, comentario){
    var deferred = $q.defer();
    utils.myHttpPost(
      API_URL + "common/multimedia/comentario/denuncia/"+idTicket,
      {comentario: comentario}
    ).then(function (API) {
      if(API.result){
        deferred.resolve(API.data);
      }
      else
        deferred.reject(utils.getError(API, "saveComentaTicket(API)"));
    },
    function(error){
      deferred.reject(utils.getError(error, "saveComentaTicket()"));
    });

    return deferred.promise;
  }

  /**
   * Una vez que se han subido los arcivos, relaciona
   * cada uno con el comentario almacenado por saveComentaTicket()
   * @param idTicket
   * @param idComentario
   * @param fileList
   * @returns {Promise}
   */
  function saveMultiComentaTicket(idTicket, idComentario, fileList){
    var deferred = $q.defer();

    utils.myHttpPost(
      API_URL +"common/multimedia/idComentario/"+idComentario+"/denuncia/"+idTicket,
      {fileList: fileList}
    ).then(function (API) {
      if(API.result)
        deferred.resolve(API.data.totalInserts);
      else
        deferred.resolve(utils.getError(API, "saveMultiComentaTicket(API)"));
    },function(error){
      deferred.reject(utils.getError(error, "saveMultiComentaTicket()"));
    });
    return deferred.promise;
  }

  return{
    updateTicketStatus:     updateTicketStatus,
    uploadFileList:         uploadFileList,
    removeFile:             removeFile,
    saveComentaTicket:      saveComentaTicket,
    saveMultiComentaTicket: saveMultiComentaTicket
  };
});

"use strict";

appServices.factory("surveyPreguntasSvc",function($q, utils){

  /**
   * Obtiene de la API las pregutnas
   * @param tipoSolicitud
   * @param idHecho
   * @param idDelito
   * @returns {promise}
   */
  function getPreguntas(tipoSolicitud, idHecho, idDelito){
    var deferred = $q.defer();

    var cadena = "/preguntas/hecho/"+idHecho+"/delito/" + idDelito;

    utils.myHttpGet(
      API_URL + "common" + cadena
    ).then(function (API) {
      if(API.result){
        var preguntas = [];

        API.data.forEach(function(item){
          preguntas.push({
            id:        item.idPregunta,
            pregunta:  item.pregunta,
            respuesta: ""
          })
        });
        deferred.resolve({items:preguntas});
      }
      else
        deferred.reject(utils.getError(API, "getPreguntas(API)"));
    },function (error) {
      deferred.reject(utils.getError(error, "getPreguntas()"))
    });

    return deferred.promise;
  }

  /**
   * Verifica si se han contestado las preguntas
   * @param form Formulario de preguntas
   * @returns {int} true:si hay al menos 1 contestada, false: en otro caso
   */
  function getNumRespuestas(form) {
    var numRespuestas = 0;
    form.forEach(function(field){
      if(field.respuesta.length){
        numRespuestas++;
      }
    });
    return numRespuestas;
  }

  function isFormEmpty(form) {
    var isEmpty = true;
    form.items.some(function(field){
      if(field.respuesta.length){
        return !(isEmpty = false);
      }
    });
    return isEmpty;
  }

  /**
   * Intenta almacenar en API la lista de respuestas
   * @param idTicket
   * @param dataToSave
   * @returns {*|promise}
   * Success:
   *   >=0: Numero de preguntas almacenadas en API
   * Error: parsedError
   */
  function savePreguntas(idTicket, dataToSave){
    var deferred = $q.defer();

    if(isFormEmpty(dataToSave)){
      deferred.resolve(-1);
      return deferred.promise;
    }

    utils.myHttpPost(
      API_URL + "common/denuncia/"+idTicket+"/catalogo/preguntas",
      dataToSave
    ).then(function (API) {
      if(API.result)
        deferred.resolve(API.data.totalInserts);
      else
        deferred.reject(utils.getError(API, "savePreguntas(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "savePreguntas()"));
    });

    return deferred.promise;
  }//<--savePreguntas()

  return{
    getPreguntas:     getPreguntas,
    getNumRespuestas: getNumRespuestas,
    isFormEmpty:      isFormEmpty,
    savePreguntas:    savePreguntas
  }

});

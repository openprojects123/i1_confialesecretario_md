"use strict";

appServices.factory("surveyObjetosSvc",function($q, utils) {
  var numCategorias = 0;

  function getObjetosCategorias() {
    var deferred = $q.defer();

    utils.myHttpGet(API_URL + "common/objeto/catCategorias")
    .then(function(API){
      if(API.result){
        numCategorias = API.data.length;
        deferred.resolve(API.data);
      }
      else
        deferred.reject(utils.getError(API, "getObjetosCategorias(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "getObjetosCategorias()"));
    });

    return deferred.promise;
  }

  function getObjetosSubcategorias(idCategoria) {
    var deferred = $q.defer();

    utils.myHttpGet(API_URL + "common/objeto/catSubcategorias/categoria/"+idCategoria)
    .then(function(API){
      if(API.result)
        deferred.resolve(API.data);
      else
        deferred.reject(utils.getError(API, "getObjetosSubcategorias(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "getObjetosSubcategorias()"));
    });

    return deferred.promise;
  }

  function addObjeto(formData, list) {
    try{
      var objeto = {
        categoria:{
          id:    formData.categoriaSel.id,
          title: formData.categoriaSel.title
        },
        subCategoria:{
          id:    formData.subCategoriaSel.id,
          title: formData.subCategoriaSel.title
        },
        cantidad:           formData.cantidad || 0,
        descripcion:        formData.descripcion || "Sin descripcion"
      };

      list.push(objeto);
      return true;
    }
    catch (ex){
      var e = utils.getError(ex);
      utils.showAlertDialog("No se pudo agregar e objeto ya que: "+e.reason);
      return false;
    }
  }

  function hasUnsavedData(form) {
    return (numCategorias &&
      (form.categoriaSel.title !== "N/A" ||
      form.subCategoriaSel.title !== "N/A" ||
      form.cantidad.length ||
      form.descripcion.length)
    )
  }

  function saveObjetos(idTicket, dataToSave){
    var deferred = $q.defer();

    if(!dataToSave.length){
      deferred.resolve(-1);
      return deferred.promise;
    }

    utils.myHttpPost(
      API_URL + "common/denuncia/"+idTicket+"/catalogo/objetos",
      dataToSave
    ).then(function (API) {
      if(API.result)
        deferred.resolve(API.data.totalInserts);
      else
        deferred.reject(utils.getError(API, "saveObjetos(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "saveObjetos()"));
    });

    return deferred.promise;
  }


  return{
    getObjetosCategorias:    getObjetosCategorias,
    getObjetosSubcategorias: getObjetosSubcategorias,
    addObjeto:               addObjeto,
    hasUnsavedData:          hasUnsavedData,
    saveObjetos:             saveObjetos
  }
});

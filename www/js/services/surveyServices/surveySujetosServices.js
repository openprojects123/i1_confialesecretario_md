"use strict";

appServices.factory("surveySujetosSvc",function($q, utils) {
  function getSujetos() {
    var deferred = $q.defer();

    utils.myHttpGet(API_URL + "common/sujeto/catsMediafiliacion")
    .then(function (API) {
      if(API.result && API.data){
        var catalogos = API.data;
        catalogos.forEach(function(c){
          c.selectedOption = c.options[0];
        });

        deferred.resolve(catalogos);
      }
      else
        deferred.reject(utils.getError(API, "getSujetos(API)"));
    }, function (error) {
      deferred.reject(utils.getError(error, "getSujetos()"));
    });
    return deferred.promise;
  }

  function addSujeto(data, list) {
    try{
      var
        sujetoGenerales       = data[0],
        sujetoCaracteristicas = data[1],
        mediaFiliacion = [];


      var generales = {
        nombre: sujetoGenerales.nombre || "Descnocido",
        alias:  sujetoGenerales.alias  || "Descnocido",
        acento: sujetoGenerales.acento || "Descnocido",
        voz:    sujetoGenerales.voz    || "Descnocido",
        rasgo:  sujetoGenerales.rasgo  || "Descnocido",
        genero: sujetoCaracteristicas[0].selectedOption.title
      };


      sujetoCaracteristicas.forEach(function(data, idx){
        if(data.selectedOption.title !== "N/A"){
          mediaFiliacion.push({
            catalogo:{
              id:   data.id,   //Id del catalogo
              title:data.title //Nombre del catalogo
            },
            opcion:{
              id:   data.selectedOption.id,  //Id de la opcion seleccionada
              title:data.selectedOption.title//Nombre de la opcion seleccionada
            }
          });

          //reset
          sujetoCaracteristicas[idx].selectedOption = sujetoCaracteristicas[idx].options[0];
        }
      });//FOREACH

      list.push({
        generales:      generales,  //objeto
        mediaFiliacion: mediaFiliacion //arreglo de objetos
      });
      return true;
    }
    catch(ex){
      var e = utils.getError(ex);
      utils.showAlertDialog("No se pudo agreagar al sujeto ya que: "+e.reason);
      return false;
    }
  }

  function hasUnsavedData(forms) {
    var
      sujetoGenerales       = forms[0],
      sujetoCaracteristicas = forms[1];

    var hasAnswers = false;
    for(var prop in sujetoGenerales){
      if(sujetoGenerales[prop].length){hasAnswers = true; break;}
    }

    var hasSelectedData = false;
    sujetoCaracteristicas.some(function(field){
      if(field.selectedOption.title !== "N/A"){
        return hasSelectedData = true;
      }
    });

    return (hasSelectedData || hasAnswers);
  }

  function saveSujetos(idTicket, dataToSave){
    var deferred = $q.defer();

    if(!dataToSave.length){
      deferred.resolve(-1);
      return deferred.promise;
    }

    utils.myHttpPost(
      API_URL + "common/denuncia/"+idTicket+"/catalogo/sujetos",
      dataToSave
    )
    .then(function (API) {
      if(API.result)
        deferred.resolve(API.data.totalInserts);
      else
        deferred.reject(utils.getError(API, "saveSujetos(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "saveSujetos()"));
    });

    return deferred.promise;
  }

  return{
    getSujetos:     getSujetos,
    addSujeto:      addSujeto,
    hasUnsavedData: hasUnsavedData,
    saveSujetos:    saveSujetos
  }

});

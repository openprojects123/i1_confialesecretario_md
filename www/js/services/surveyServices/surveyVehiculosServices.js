"use strict";

appServices.factory("surveyVehiculosSvc",function($q, utils){
  var numMarcas = 0;
  /**
   * Obtiene todas las marcas
   * @returns {promise}
   */
  function getMarcas(){
    var deferred = $q.defer();

    utils.myHttpGet(
      API_URL + "common/vehiculo/catMarcas"
    ).then(function (API) {
      if(API.result){
        numMarcas = API.data.length;
        deferred.resolve(API.data);
      }
      else
        deferred.reject(utils.getError(API, "getMarcas(API)"));
    }, function (error) {
      deferred.reject(utils.getError(error, "getMarcas()"));
    });

    return deferred.promise;
  }

  /**
   * Obtiene Todos los modelos de x marca
   * @param marca
   * @returns {promise}
   */
  function getModelos(idMarca) {
    var deferred = $q.defer();

    utils.myHttpGet(
      API_URL + "common/vehiculo/catModelos/marca/"+idMarca
    ).then(function (API) {
      if(API.result){
        deferred.resolve(API.data);
      }
      else
        deferred.reject(utils.getError(API), "getModelos(API)");
    }, function (error) {
      deferred.reject(utils.getError(error, "getModelos()"));
    });

    return deferred.promise;
  }

  /**
   * Obtiene todos los colores disponibles de vehiculos
   * @returns {promise}
   */
  function getColores() {
    var deferred = $q.defer();

    utils.myHttpGet(
      API_URL + "common/vehiculo/catColores"
    ).then(function (API) {
      if(API.result){
        deferred.resolve(API.data);
      }
      else
        deferred.reject(utils.getError(API, "getColores(API)"));
    }, function (error) {
      deferred.reject(utils.getError(error, "getColores()"));
    });

    return deferred.promise;
  }

  function addVehiculo(data, list) {
    try{
      var currentVehiculo = {
        marca:{
          id:   data.marcaSel.id,
          title:data.marcaSel.title
        },
        modelo:{
          id:    data.modeloSel.id,
          title: data.modeloSel.title
        },
        color:{
          id:    data.colorSel.id,
          title: data.colorSel.title
        },
        anio:        data.anio || "N/A",
        placa:       data.placas,
        descripcion: data.descripcion
      };

      list.push(currentVehiculo);
      return true;
    }
    catch(ex){
      var e = utils.getError(ex);
      utils.showAlertDialog("No se pudo agreagar al vehículo ya que: "+e.reason);
      return false;
    }
  }

  function hasUnsavedData(form) {
    return (numMarcas>0 &&
      (form.marcaSel.title !== "N/A" ||
      form.modeloSel.title !== "N/A" ||
      form.colorSel.title  !== "N/A" ||
      form.anio.length ||
      form.placas.length ||
      form.descripcion.length)
    )
  }

  function saveVehiculos(idTicket, dataToSave){
    var deferred = $q.defer();

    if(!dataToSave.length){
      deferred.resolve(-1);
      return deferred.promise;
    }

    utils.myHttpPost(
      API_URL + "common/denuncia/"+idTicket+"/catalogo/vehiculos",
      dataToSave
    )
    .then(function (API) {
      if(API.result)
        deferred.resolve(API.data.totalInserts);
      else
        deferred.reject(utils.getError(API, "saveItems(API)"));
    }, function(error){
      deferred.reject(utils.getError(error, "saveItems()"));
    });

    return deferred.promise;
  }//<--saveItems()

  return{
    getMarcas:      getMarcas,
    getModelos:     getModelos,
    getColores:     getColores,
    addVehiculo:    addVehiculo,
    hasUnsavedData: hasUnsavedData,
    saveVehiculos:  saveVehiculos
  }

});

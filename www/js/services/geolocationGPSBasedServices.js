"use strict";

appServices.factory("gpsLocationSvc", function($rootScope, $q, utils){

  var isGooglePlayConnected  = false, //Conectado a GooglePlay?
      is_iOSConnected        = false,
      bgGeo; //<-- objeto de ios para bgLocation


  /**
   * @description
   * Inicializa el servicio (plugin) android/ios de geolocalizacion,
   * y solicita una localizacion inicial.
   * Tambien se usa solo para activar el dialogo de permisos en iOS.
   * @returns {Promise}
   * Success: (Object) latlng
   * Fail: (Object) parsedError
   */
  function init(){
    var deferred = $q.defer();
    try{
      if($rootScope.PLATFORM.isAndroid()){
        //Verificar conexion a GooglePlay...
        if(!window.pluginTest){
          deferred.reject(
            utils.getError("El plugin de Geolocalización no se cargó", "init()")
          );
          return deferred.promise;
        }

        utils.putMsg.info("intentando inicialiar GooglePlay");

        window.pluginTest.init(function(data){
          isGooglePlayConnected = true;
          utils.putMsg.log(data);
          deferred.resolve(data);
        }, function(reason){
          deferred.reject(
            utils.getError(
              {
                statusText: reason,
                reason:    "No se pudo conectar con GooglePlay"
              },
              "pluginTest.init()"
            )
          );
        });
      }//<-- isAndroid()
      else if($rootScope.PLATFORM.isIOS() || $rootScope.PLATFORM.isIPad()){
        utils.putMsg.info("intentando inicialiar BackgroundGeolocation");
        bgGeo = window.BackgroundGeolocation;

        // BackgroundGeoLocation is highly configurable.
        bgGeo.configure({
          // Geolocation config
          desiredAccuracy: 0,
          distanceFilter: 10,
          stationaryRadius: 10,
          //locationUpdateInterval: 1000, //<-- Android
          //fastestLocationUpdateInterval: 5000, //<-- Android
          locationAuthorizationRequest: "Always",//<--ios

          // Activity Recognition config
          activityType: 'AutomotiveNavigation',
          activityRecognitionInterval: 5000,
          stopTimeout: 5,

          // Application config
          debug: true,
          stopOnTerminate: true,

        },
          function(state) {
            // This callback is executed when the plugin is ready to use.
            is_iOSConnected = true;

            utils.putMsg.log('BackgroundGeolocation Iniciada!');

            if (!state.enabled) {
              // Listen to location events & errors.
              bgGeo.on('location', iOS_SuccessFn, iOS_FailureFn);

              /*/ Fired whenever state changes from moving->stationary or vice-versa.
              bgGeo.on('motionchange', function(isMoving) {
               utils.putMsg.info('- onMotionChange: '+ isMoving);
              });*/

              //solo para activar el dialogo de permisos...
              utils.putMsg.info("Ejecutando geolocalización inicial");
              bgGeo.getCurrentPosition(
                function(location, taskId) {
                  bgGeo.finish(taskId);
                  utils.putMsg.info("Geolocalización inicial completada.");
                }, function(errorCode) {
                  var err = "No se pudo acceder a los servicios de geolocalización, errorCode: "+errorCode;
                  utils.showAlertDialog(err);
                  utils.getError(err, "bgGeo.getCurrentPosition()");
                }
              );//<-- solo para activar el dialogo de permisos
            }
            deferred.resolve(true);
          }
        );//<--bgGeo.configure()
      }//<--isIOS()
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }
    return deferred.promise;
  }

  /**
   * @description
   * Obtiene la geolocalizacion sólo una vez
   * @returns {Promise}
   * Success: (Object) latlng
   * Fail: (Object) parsedError
   */
  function getLocationOnce(){
    var deferred = $q.defer();

    utils.putMsg.info("Obteniendo su ubicación");
    try{
      /*/fixme*******************************************************************
      //*                             PARA TESTING                             *
      //************************************************************************
       utils.putMsg.error("*** getLocationOnce() en modo DEBUG!");
      utils.timeout(function () {
        deferred.resolve({lat: 123, lng: 456, speed: null});
       utils.putMsg.success("Fin getLocationOnce() en modo DEBUG!");
      }, 5*ONE_SECOND);
      return deferred.promise;
      //************************************************************************/

      if($rootScope.PLATFORM.isAndroid()){
        window.pluginTest.start(
          function(location){
            window.pluginTest
            .stop(function(resp){
              utils.putMsg.info("Geolocalización Finalizada: "+JSON.stringify(resp));
              deferred.resolve(location);
            }, function(reason){
              utils.putMsg.warn("Error al intentar detener la geolocalización."+reason);
              deferred.resolve(location);
            });
          },
          function(reason){
            deferred.reject(utils.getError(reason, "getLocationOnce()"));
          });
      }
      else if($rootScope.PLATFORM.isIOS() || $rootScope.PLATFORM.isIPad()){
        bgGeo.getCurrentPosition(
        function(location, taskId) {
          // Must signal completion of your iOS_SuccessFn.
          bgGeo.finish(taskId);
          //utils.putMsg.info(JSON.stringify(location));

          bgGeo.stop(function(){
            utils.putMsg.info("Geolocalización Finalizada.");
            deferred.resolve({lat: location.coords.latitude, lng: location.coords.longitude, speed: location.coords.speed});
          }, function(reason){
            utils.putMsg.warn("Error al intentar detener la geolocalización: "+reason);
            deferred.resolve({lat: location.coords.latitude, lng: location.coords.longitude, speed: location.coords.speed});
          });
        },
        function(errorCode) {
          deferred.reject(utils.getError('errorCode: '+errorCode, "getLocationOnce()"));
        },
        {
          timeout:         30,  // 30 second timeout to fetch location
          maximumAge:      5000,// Accept the last-known-location if not older than 5000 ms.
          desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
          samples:         3    // How many location samples to attempt.
        });
      }
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }


  /**
   * @description
   * This callback will be executed every time a geolocation
   * is recorded in the background.
   * @param location
   * @param taskId
   */
  function iOS_SuccessFn(location, taskId) {
    try{
      // Must signal completion of your iOS_SuccessFn.
      utils.putMsg.info("****** Ejecutando bgGeo.finish("+taskId+")");
      bgGeo.finish(taskId);
    }
    catch(e){
      utils.getError(e);
    }
  }

  /**
   * @description
   * This callback will be executed if a location-error occurs.
   * Eg: this will be called if user disables location-services.
   * @param errorCode
   */
  function iOS_FailureFn(errorCode) {
    utils.putMsg.error("iOS_FailureFn(), errorCode: "+errorCode);
    //utils.timeout(locationLoop, 1000*30);
  }

  function isAndroidConnected() {
    return isGooglePlayConnected;
  }

  function isIOSConnected() {
    return is_iOSConnected;
  }

  return{
    init:               init,
    getLocationOnce:    getLocationOnce,
    isAndroidConnected: isAndroidConnected,//Conectado a GooglePlay?
    isIOSConnected:     isIOSConnected
  };
});

"use strict";

appServices
.factory('diagnosticSvc', ["$q", "$ionicPlatform", "utils",
  function($q, $ionicPlatform, utils){
    var d = null;
    $ionicPlatform.ready(function(){
      document.addEventListener("deviceready",function(){
        d = cordova.plugins.diagnostic;
      }, false);
    });

    function isOnline() {
      /*var networkState = navigator.connection.type;
      var states = {};
      states[Connection.UNKNOWN] = 'Unknown connection';
      states[Connection.ETHERNET] = 'Ethernet connection';
      states[Connection.WIFI] = 'WiFi connection';
      states[Connection.CELL_2G] = 'Cell 2G connection';
      states[Connection.CELL_3G] = 'Cell 3G connection';
      states[Connection.CELL_4G] = 'Cell 4G connection';
      states[Connection.CELL] = 'Cell generic connection';
      states[Connection.NONE] = 'No network connection';

      var networkType = states[networkState];*/
      //alert('Connection type: ' + states[networkState]);
      return navigator.connection.type !== Connection.NONE;
    }

    /**
     * @description
     * Verifica si el GPS está habilitado
     * @returns {Promise}
     */
    function isLocationEnabled(){
      var deferred = $q.defer();
      try{
        d.isLocationEnabled(function(resp){
          deferred.resolve(resp);
        }, function(error){
          deferred.reject(utils.getError(error, "isLocationEnabled()"));
        });
      }
      catch(e){
        deferred.reject(utils.getError(e));
      }
      return deferred.promise;
    }

    function switchToLocationSettings(){
      try{
        d.switchToLocationSettings();
      }
      catch(e){
        utils.putMsg.error(utils.getError(e));
      }
    }

    /**
     * @description
     * Verifica si la tarjeta WiFi está habilitada,
     * No verifica que haya datos (internet)
     * @returns {Promise}
     */
    function isWifiEnabled(){
      var deferred = $q.defer();
      try{
        d.isWifiEnabled(function(resp){
          deferred.resolve(resp);
        }, function(error){
          deferred.reject(utils.getError(error));
        });
      }
      catch(e){
        deferred.reject(utils.getError(e));
      }
      return deferred.promise;
    }

    function switchToWifiSettings(){
      try{
        d.switchToWifiSettings();
      }
      catch(e){
        utils.putMsg.error(utils.getError(e));
      }
    }

    return {
      isOnline:                 isOnline,
      isLocationEnabled:        isLocationEnabled,
      switchToLocationSettings: switchToLocationSettings,
      isWifiEnabled:            isWifiEnabled,
      switchToWifiSettings:     switchToWifiSettings
    };
}]);

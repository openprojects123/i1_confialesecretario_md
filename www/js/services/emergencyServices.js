"use strict";

appServices.factory("emergencySvc", function ($rootScope, $q, gpsLocationSvc, trackingSvc,
                                              utils) {
  var
    tipoEmergencia = null,
    points         = [],  //Puntos del tracking a almacenar
    savedId        = 0,   //El id de la emergencia almacenada
    numLocations   = 0;   //contador para display de mensajes

  /**
   * @description
   * Obtiene una geolocalizacion y envia una emergencia
   * @param tipo (intger|string) id tipo emergencia
   * @returns {Promise}
   * Success: (integer)savedId
   * Fail: (Object) parsedError
   */
  function sendSingleEmergency(tipo) {
    var deferred = $q.defer();

    tipoEmergencia = tipo;
    gpsLocationSvc.getLocationOnce()
    .then(function (location) {
      sendEmergency(location)
      .then(function (resp) {
        deferred.resolve(resp);
      }, function (error2) {
        deferred.reject(error2);
      });/*.finally(function () {
        tipoEmergencia = null;
        savedId        = null;
        numLocations   = 0;
        points         = [];
      });*/
    }, function (error) {
      deferred.reject(error);
    }).finally(function () {
      tipoEmergencia = null;
      savedId        = null;
      numLocations   = 0;
      points         = []
    });

    return deferred.promise;
  }

  /**
   * @description
   * Obtiene X geolocalizaciones y envia/actualiza
   * una solicitud de emergencia
   * @param tipo (intger|string) id tipo emergencia
   */
  function startEmergencyAndTracking(tipo) {
    tipoEmergencia = tipo;
    if(!$rootScope.IS_TRACKING)
      trackingSvc.startTrackingLoop(30*ONE_SECOND, sendEmergency, function(_){});
  }

  /**
   * @description
   * Intenta detener el loop de geolocalizacion
   * @returns {Promise}
   * Success: true
   * Fail (Object) parsedError
   */
  function stopEmergencyAndTracking() {
    var deferred = $q.defer();
    trackingSvc.stopTrackingLoop()
    .then(function (resp) {
      savedId        = 0;
      tipoEmergencia = null;
      savedId        = null;
      numLocations   = 0;
      //points         = [];
      deferred.resolve(true);
    }, function (error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  /**
   * @description
   * Intenta enviar una solicitud de emergencia
   * @param location (Object) {lat, lng, speed}
   * @returns {Promise}
   * Success: (Object) respuesta del servidor (?)
   * Fail: (Object) parsedError
   */
  function sendEmergency(location) {
    var deferred = $q.defer();

    try{
      var Hoy  = utils.fecha().hoy;
      var hora = utils.fecha().currtime;

      points.push({
        lat:   location.lat,
        lng:   location.lng,
        vel:   location.speed >= 0 ? (location.speed*3.6): location.speed,// *3.6 = m/s to km/h
        fecha: Hoy + " " + hora
      });

      var formData = {
        points:         JSON.stringify(points),
        idUsuario:      $rootScope.SESSION_ATTRS.getUserId(),
        tipoEmergencia: tipoEmergencia,//i.e. (integer) id | (string)
        savedId:        savedId
      };

      /*/Fixme:******************************************************************
       //*                             FOR TESTING                              *
       //************************************************************************
       utils.putMsg.error("******** sendEmergency() in debug");
       utils.putMsg.info("location:"+JSON.stringify(location));
       utils.timeout(function () {
       deferred.resolve(-99);
       utils.putMsg.success("Fin sendEmergency() en modo DEBUG!")
       },5*ONE_SECOND);
       return deferred.promise;
       /**************************************************************************/

      utils.myHttpPost(
        API_URL + "public/emergencia",
        formData
      ).then(function(API){
        if(API.result){
          numLocations++;
          if(formData.savedId === 0){
            savedId = API.data.idEmergencia;
            utils.putMsg.info("1) Ubicación almacenada con ID: "+ API.data.idEmergencia);
          }
          else
            utils.putMsg.info(numLocations+") Nueva Ubicación almacenada");

          deferred.resolve(API.data);
        }
        else
          deferred.reject(utils.getError(API, "sendEmergency(API)"));
      },
      function(error){
        deferred.reject(utils.getError(error), "sendEmergency()");
      });
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  function getTrackedPoints() {
    return points;
  }

  return{
    sendSingleEmergency:       sendSingleEmergency,
    startEmergencyAndTracking: startEmergencyAndTracking,
    stopEmergencyAndTracking:  stopEmergencyAndTracking,
    getTrackedPoints:          getTrackedPoints
  }
});

"use strict";

appServices
.factory("utils", ["$rootScope", "$q", "$http", "$timeout", "$ionicPopup",
  "$ionicLoading", "$cordovaToast", "$sce", "$mdDialog", "$mdToast", "$mdBottomSheet", "$ionicScrollDelegate",
function($rootScope, $q, $http, $timeout, $ionicPopup, $ionicLoading, $cordovaToast,
         $sce, $mdDialog, $mdToast, $mdBottomSheet, $ionicScrollDelegate){

  var
    log = localStorage.getItem("log") || "",
    consoleContainer;

  console.info(getCompFecha().full);
  log += "<span style='color: lime'> START: "+getCompFecha().full+"</span><br>";

  $timeout(function () {
    consoleContainer = document.querySelector("#appLogContent");
    $rootScope.APP_LOG = $sce.trustAsHtml(log);
  }, ONE_SECOND);


  function popup(data){
    var p = {};

    if(typeof data === 'string'){
      p.content = data;
    }
    else{
      p = data;
    }

    var config = {
      cssClass:    p.class,
      title:       p.title,
      template:    p.template,
      templateUrl: p.templateUrl,
      content:     p.content,
      okText:      p.okText || "Aceptar",
      scope:       p.scope
    };

    if(typeof p.buttons === 'object'){
      config.buttons = p.buttons;
    }

    //por si NO necesito los botones (fake-toast, file-progress, etc)
    if(p.buttons === false){
      config.buttons = [];
      config.okText  = null;
    }

    return $ionicPopup.alert(config);
  }

  function showConfirmDialog(msg, config){
    var deferred = $q.defer();
    if(!config)config = {};
    $mdDialog.show({
      controller:"DialogController",
      templateUrl:"templates/partials/dialogsPopupTpl.html",
      //targetEvent:e,
      locals:{
        displayOption:{
          title:   config.title || "Confirmar acción",
          content: msg,
          ok:      config.ok || "Aceptar",
          cancel:  config.cancel || "Cancelar"
        }
      }
    })
      .then(function(){
        deferred.resolve(true);
      },function(){
        deferred.reject(false);
      });
    return deferred.promise;
  }

  function showAlertDialog(msg, config){
    var deferred = $q.defer();
    if(!config)config = {};
    $mdDialog.show({
      controller:"DialogController",
      templateUrl:"templates/partials/dialogsPopupTpl.html",
      //targetEvent:e,
      locals:{
        displayOption:{
          title:   config.title || "Advertencia",
          content: msg,
          ok:      config.ok || "Aceptar",
          cancel:""
        }
      }
    }).then(function(){
      deferred.resolve(true);
    });
    return deferred.promise;
  }

  function showToast(msg, delay, pos){
    $mdToast.show({
      hideDelay: delay || 2500,
      position: 'top',
      controller:"toastController",
      templateUrl:"templates/partials/toastTpl.html",
      locals:{
        displayOption:{message: $sce.trustAsHtml(msg)}
      }
    });
  }

  function showListBottomSheet(e){
    $mdBottomSheet.show({
      templateUrl:"templates/partials/listBottomSheetTpl.html",
      //targetEvent:e,
      //scope:$scope.$new(!1)
    });
  }

  function closeListBottomSheet(){$mdBottomSheet.hide();}

  function showGridBottomSheet(e){
    $mdBottomSheet.show({
      templateUrl:"templates/partials/gridBottomSheetTpl.html",
      //targetEvent:e,
      //scope:$scope.$new(!1)
    });
  }

  function showLoading(msg){
    $ionicLoading.show({
      //template: msg,
      content: msg,
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
  }

  function hideLoading(){
    $ionicLoading.hide();
  }

  function timeout(funct, delay){
    if(!delay) delay = ONE_SECOND;
    $timeout(funct, delay);
  }

  function speech(msg){
    var deferred = $q.defer();

    if(typeof TTS != 'undefined' || !TTS){
      showToast(msg, 3*ONE_SECOND);
      return;
    }

    TTS.speak({
      text: msg,
      locale: 'es-MX',
      rate: (ionic.Platform.isAndroid() ? 1.4 : 1.0)
    }, function () {
      deferred.resolve("TTS ok");
    }, function (reason) {
      showToast(msg);
      deferred.reject(getError("ERROR en TTS: " + reason, "speech()"));
    });

    return deferred.promise;
  }

  /**
   * @description
   * Helper para (intentar) obtener el archivo y la linea en que
   * se ejecuto la funcion que invoca a getError() y putMsg.xxxx()
   * @returns {*}
   */
  function getErrorObject(){
    try { throw Error(''); } catch(e) {return e; }
    /*Otra forma...
    var callerName;
    var
      re = /(\w+)@|at (\w+) \(/g,
      aRegexResult = re.exec(new Error().stack);
    console.log(aRegexResult);
    callerName = aRegexResult.input.split("\n")[2];
    callerName = callerName.substring(callerName.indexOf(".")+1);
    callerName = callerName.substring(0, callerName.indexOf(" "))+"()";
    //console.log(callerName);*/
  }

  /**
   * @description
   * Helper que invoca a writMSG
   * @type {{
   *  error: putMsg.error,
   *  warn: putMsg.warn,
   *  log: putMsg.log,
   *  success: putMsg.success,
   *  info: putMsg.info
   *  }}
   */
  var putMsg = {
    //error: Other runtime errors or unexpected conditions.
    //Expect these to be immediately visible on a status console.
    //No importa el nivel del debugger, todos se almacenan
    error: function(msg){writeMSG(msg, "error");},

    //warn - Use of deprecated APIs, poor use of API, 'almost' errors, other runtime
    //situations that are undesirable or unexpected,  but not necessarily "wrong".
    //Expect these to be immediately visible on a status console.
    warn: function(msg){
      if(
        LEVEL_DEBUGGER == 'warn' || LEVEL_DEBUGGER == 'log' ||
        LEVEL_DEBUGGER == 'success' || LEVEL_DEBUGGER == 'info'
      )
        writeMSG(msg, "warn");
    },

    //log - detailed information on the flow through the system.
    //Expect these to be written to logs only.
    log: function(msg){
      if(
        LEVEL_DEBUGGER == 'log' || LEVEL_DEBUGGER == 'success' ||
        LEVEL_DEBUGGER == 'info'
      )
        writeMSG(msg, "log");
    },

    success: function(msg){
      if(LEVEL_DEBUGGER == 'success' || LEVEL_DEBUGGER == 'info')
        writeMSG(msg, "success");
    },

    //info - Interesting runtime events (startup/shutdown).
    //Expect these to be immediately visible on a console,
    //so be conservative and keep to a minimum.
    info: function(msg){if(LEVEL_DEBUGGER == 'info')writeMSG(msg, "info");}

    //trace - more detailed information. Expect these to be written to logs only.
    //trace:    function(msg){writeMSG(msg, "orangered");}
  };

  /**
   * @description
   * Obtiene un trace simple de la funcion invocadora cuando no se pasa
   * el param where y dependiendo de los settings, escribe en localstorag y/o
   * consola js del explorador web.
   * Todos los type = 'error' se almacenan en localstorage y se imprimen en consola
   * @param msg: string el mensaje
   * @param type: string {info, success, log, warn, error}
   * @param where: string (opconal) funcion invocadora
   */
  function writeMSG(msg, type, where) {
    if(!msg)
      msg = 'null';
    else if(typeof msg === 'undefined')
      msg = 'undefined';

    var
      offset = navigator.userAgent.indexOf("Android"),//5==android, 3==ios
      errStr   = getErrorObject().stack.split("\n")[offset>=0?5:3].trim(),
      when     = getCompFecha().currtime,
      fromFile = errStr.substring(errStr.lastIndexOf("/")+1, errStr.lastIndexOf(":")),
      fromFunction ='';
    //console.log(navigator.userAgent)
    //console.log(getErrorObject().stack.split("\n")[5].trim())
    //console.log(offset)
    //console.log(errStr)

    var isAnonymous  = offset >= 0 ? errStr.indexOf("anonymous") !=-1 : errStr.indexOf("@") == -1;
    var isInFunction = (errStr.indexOf(" ") != errStr.lastIndexOf(" "));
    //console.log(isAnonymous, isInFunction)

    if(typeof where !== 'undefined')
      fromFunction = where+" ";
    else if(!isAnonymous && isInFunction){
      if(offset>=0){
        //console.log("Android")
        errStr = errStr.substring(
          errStr.indexOf("at")+3,
          errStr.indexOf(" ", errStr.indexOf("at")+3)
        );

        //por si es object.nombreDeFuncion
        fromFunction = errStr.substring(errStr.indexOf(".")+1)+"() ";
      }
      else{
        //console.log("IOS")
        errStr = errStr.substring(
          0,
          errStr.indexOf("@")-1
        );
        //por si es object.nombreDeFuncion
        fromFunction = errStr.substring(errStr.indexOf(".")+1)+"() ";
      }
    }

    var maxInfoLength = 320;
    if(msg.length > maxInfoLength)
      msg = msg.substring(0, maxInfoLength)+"...";

    var errorForConsole = fromFunction+msg+". "+fromFile;

    log += "["+type.toUpperCase()+": "+when+
      "<span class='log-fromFunction'> "+(fromFunction ? fromFunction:'')+"</span>"+"] "+
      "{<span class='log-message-"+type+"'>"+msg+"</span>} "+
      "<span class='log-fromFile'>"+fromFile+"</span>"+"<br>";



    //Solo errores se almacenan cuando NO esta en modo debug
    if(DEBUG_MODE===false && (type === "error")){
      console.error(errorForConsole);
      localStorage.setItem("log", log);
      return;
    }


    //Todos (los que apliquen) se almacenan cuando SI está modo debug
    localStorage.setItem("log", log);

    switch (type){
      case "error":
        console.error(errorForConsole);
        break;
      case "warn":
        console.warn(errorForConsole);
        break;
      default://ya que los .info no se se imprimen en xcode :(
        console.log(errorForConsole);//log, succes, info
    }

    //Declaracion hasta arriba!
    if(!consoleContainer) return;

    $rootScope.APP_LOG = $sce.trustAsHtml(log);

    $ionicScrollDelegate.$getByHandle('consoleScroll').scrollBottom();

  }//<--writeMSG()

  /**
   * @description
   * Devuelve un objeto Error obtenido usando composeError(error)
   * y llama a writeMSG para almacenar los mensaje en localstorage
   * @param error {string | Object}
   * @param where (opcional) {string} nombre de funcion que invocadora
   * @returns {{date, status, statusText, reason, fullError}|*}
   */
  function getError(error, where){

    var errorObj = composeError(error);

    writeMSG(errorObj.fullError, "error", where);

    if(!errorObj.statusText && !errorObj.reason)
      putMsg.error("Error original-> "+JSON.stringify(error));

    return errorObj;
  }

  /**
   * @description
   * Devuelve un objeto Error obtenido usando composeError(error)
   * pero NO llama a writeMSG, por lo que no se almacenan en localstorag los mensajes
   * @param error {string | Object}
   * @returns {{date, status, statusText, reason, fullError}|*}
   */
  function extractError(error) {
    return composeError(error);
  }

  /**
   * @description
   * Helper que crea un objeto con mensajes de error
   * @param error (Object | string)
   * @returns {{
   *    date: string,
   *    status: number,
   *    statusText: string,
   *    reason: string,
   *    fullError: string
   *  }}
   */
  function composeError(error) {
    var
      status     = -99,//$HTTP Status
      statusText = "", //$HTTP Status Text
      reason     = ""; //errores enviados por el servicio $HTTP

    if(typeof error === "string"){//Error personalizado o de algun plugin/servicio
      status     = -98;
      statusText = "App Error";
      reason     = error;
    }
    else if(typeof error === "object"){

      if(error.error_message){//Errores de la API (webserver)
        status     = error.status || -99;
        statusText = error.statusText;
        reason     = error.error_message
      }
      else if(error.message){//Errores del bloque try-catch
        status     = -99;
        statusText = error.message;
        reason     = "Hubo un error interno en la aplicación";
      }
      else if(error.status >= 400 && error.status <500){//Errores 4**
        status     = error.status;
        statusText = error.statusText;
        reason     = "No se encontró el recurso solicitado";}
      else if(error.status >= 500 && error.status <600){//Errores 5**

        status     = error.status + ": "+
          ((error.data && error.data.api_exception) ? error.data.api_exception : "Error no especificado");
        statusText = error.statusText;
        reason     = "Hubo un error interno en el servidor remoto";
      }
      else if(error.reason){//Errores personalizados (por mi)
        status     = -100;
        statusText = error.statusText || "Error desconocido en algun plugin/servicio";
        reason     = error.reason
      }
      else if(error.status === -1){
        status     = -1;
        statusText = "Hubo un error desconocido";
        reason     = "Es posible que el internet se haya desconectado o que el servidor haya rechazado explícitamente la conexión";
      }


    }//<-- (typeof error === Object)

    //TODO implementarlo como toString()
    var fullError =
      "status: "      +status+
      "; statusText: "+(statusText || "**Estatus Desconocido")+
      "; reason: "    +(reason     || "**Ocurrio un error desconocido");

    return {
      date:       getCompFecha().currtime,
      status:     status,
      statusText: statusText || "**Estatus Desconocido",
      reason:     reason     || "**Error Desconocido",
      fullError:  fullError
    };
  }

  function clearConsole() {
    $rootScope.APP_LOG = "";
    localStorage.setItem("log", "");
    log = "";
  }

  function myHttpGet(url){
    var deferred = $q.defer();
    try{
      $http.get(url)
      .then(function(data){
        deferred.resolve(data.data);
      }, function (error){
        deferred.reject(error);
      });
    }
    catch(e){
      deferred.reject(e.message);
    }
    return deferred.promise;
  }

  function myHttpPost(url, data, headers){
    var deferred = $q.defer();
    try{
      $http({
        method:  "POST",
        url:     url,
        headers: headers || {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data:    data
      })
        .then(function(data){
          deferred.resolve(data.data);
        }, function(error){
          deferred.reject(error);
        });
    }
    catch(e){
      deferred.reject(e);
    }
    return deferred.promise;
  }

  function myHttpPut(url){
    var deferred = $q.defer();
    try{
      $http.put(url)
        .then(function(data){
          deferred.resolve(data.data);
        }, function(error){
          deferred.reject(error);
        });
    }
    catch(e){
      deferred.reject(e);
    }
    return deferred.promise;
  }

  function myHttpDelete(url){
    var deferred = $q.defer();
    try{
      $http.put(url)
        .then(function(data){
          deferred.resolve(data.data);
        }, function(error){
          deferred.reject(error);
        });
    }
    catch(e){
      deferred.reject(e);
    }
    return deferred.promise;
  }

  function getCompFecha(){
    //putMsg.log("calculando fecha actual...");
    var FECHA    = new Date();
    var MES      = ("0"+(FECHA.getMonth()+1)).slice(-2);
    var DIA      = ("0" + FECHA.getDate()).slice(-2);
    var HORAS    = FECHA.getHours();
    var MINUTOS  = FECHA.getMinutes();
    var SEGUNDOS = FECHA.getSeconds();
    var MILISEGUNDOS = FECHA.getMilliseconds();
    var TIME     = FECHA.getTime();
    var CURRTIME = FECHA.toLocaleTimeString("es-MX");
    var HOY      = (FECHA.getFullYear() + "-" + MES + "-" + DIA);

    return {
      full:     FECHA,
      anio:     FECHA.getFullYear(),
      mes:      MES,
      dia:      DIA,
      horas:    HORAS,
      minutos:  MINUTOS,
      segundos: SEGUNDOS,
      mSegundos: MILISEGUNDOS,
      time:     TIME,
      currtime: CURRTIME,
      hoy:      HOY
    };
  };

  return {
    popup:                popup,
    showLoading:          showLoading,
    hideLoading:          hideLoading,
    showAlertDialog:      showAlertDialog,
    showConfirmDialog:    showConfirmDialog,
    showToast:            showToast,
    showListBottomSheet:  showListBottomSheet,
    closeListBottomSheet: closeListBottomSheet,
    showGridBottomSheet:  showGridBottomSheet,
    timeout:              timeout,
    speech:               speech,
    putMsg:               putMsg,
    getError:             getError,
    extractError:         extractError,
    clearConsole:         clearConsole,
    myHttpGet:            myHttpGet,
    myHttpPost:           myHttpPost,
    myHttpPut:            myHttpPut,
    fecha:                getCompFecha
  };
}]);

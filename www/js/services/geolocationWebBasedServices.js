"use strict";

appServices.factory("webLocationSvc", function($q, utils){
  var fullDir = "";

  /**
   * @description
   * Obtiene las coordenadas de la ubicacion actual
   * @returns {Promise}
   * Success: (Object){lat:~, lng:~}
   * Fail: (Object) parsedError
   */
  function getLocationOnce(){
    var deferred = $q.defer();

    try{
      navigator.geolocation.getCurrentPosition(function (position) {
        //utils.putMsg.info("getLocationOnce: "+JSON.stringify(position));
        deferred.resolve({lat: position.coords.latitude, lng: position.coords.longitude});
      }, function(error){
        if (error.code === 1){
          deferred.reject(utils.getError(
            "No se otorgaron los permisos suficientes para usar la geolocalización"
          ), "getLocationOnce()");
        }
        else if (error.code === 2){
          deferred.reject(utils.getError(
            "Uno o más elementos de la geolocalización fallaron"
          ), "getLocationOnce()");
        }
        else if (error.code === 3){
          deferred.reject(utils.getError(
            "Se agotó el tiempo de espera de la petición"
          ), "getLocationOnce()");
        }
        else{
          deferred.reject(utils.getError(error, "getLocationOnce()"));
        }

      });
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }
    return deferred.promise;
  }

  /**
   * @description
   * Obtiene la Latitud y longitud dados los prametros en data:
   * {estado, municipio, colonia, calle}
   * @param data
   * @returns {Promise}
   * Success: (Array) [lat, lng]
   * Fail: (Object) parsedError
   */
  function reverseCoding(data){
    var deferred = $q.defer();

    utils.putMsg.info("reverseCoding...");
    //Documentacion:::  https://developers.google.com/maps/documentation/geocoding/intro

    //utils.putMsg.info("reverseCoding->"+ JSON.stringify(data));

    var c1, c2, c3, c4;
    c1 = (data.calle.length)  ? data.calle.replace(/\s+/g, "+") + "," : "";
    c2 = (data.colonia.length)? data.colonia.replace(/\s+/g, "+") + "," : "";
    c3 = data.nomMunicipio.replace(/\s+/g, "+") + ",";
    c4 = data.nomEstado.replace(/\s+/g, "+");

    var geocoder = "https://maps.google.com/maps/api/geocode/json?address=";
    geocoder += c1 + c2 + c3 + c4;

    //utils.putMsg.info( "Dir:\n"+geocoder);

    utils.myHttpGet(
      geocoder
    ).then(function(obj){
      utils.putMsg.info("reverseCoding->reverseCoding()->status: "+ obj.status);
      if(obj.results.length){
        var pos = obj.results[0].geometry.location;
        deferred.resolve(pos);
      }
      else
        deferred.reject("No se encontraron coordenadas para la dirección dada", "reverseCoding()");
    }, function(error){
      deferred.reject(error, "reverseCoding()");
    });

    return deferred.promise;
  }

  /**
   * @description
   * Obtiene los componentes de una direccion (los que se puedan):
   * [estado, municipio, colonia, calle, etc]
   * @param latLng
   * @returns {Promise}
   * Success: (Array) [componentes_direccion]
   * Fail: (Object) parsedError
   */
  function getFullAddress(latLng){
    var deferred  = $q.defer();

    utils.putMsg.info("getting Full Address");

    function reverseGeocoderSuccess(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // Display address as text in the page
          //utils.putMsg.info("Address: ", results[0].formatted_address);
          var direccion = new Object();
          var result = results[0].address_components;
          for(var i in result){
            /*utils.putMsg.info("type = " + result[i].types[0] + " long_name = " +
             result[i].long_name);*/
            if(result[i].types[0] === 'country')
              direccion["pais"] = result[i].long_name;
            if(result[i].types[0] === 'state')
              direccion["estado"] = result[i].long_name;
            if(result[i].types[0] === 'administrative_area_level_1')
              direccion["areaLvl1"] = result[i].long_name;//como el estado
            if(result[i].types[0] === 'locality')
              direccion["municipio"] = result[i].long_name;
            if(result[i].types[0] === 'administrative_area_level_2')
              direccion["areaLvl2"] = result[i].long_name;//como el municipio...
            if(result[i].types[0] === 'route')
              direccion["calle"] = result[i].long_name;
            if(result[i].types[0] === "political")
              direccion["colonia"] = result[i].long_name;
            if(result[i].types[0] === 'street_number')
              direccion["calleNumero"]= result[i].long_name;
            if(result[i].types[0] === 'postal_code')
              direccion["codigoPostal"] = result[i].long_name;
          }

          if(!direccion["estado"]){direccion["estado"] = direccion["areaLvl1"];}
          if(!direccion["municipio"]){direccion["municipio"] = direccion["areaLvl2"];}

          if(direccion["colonia"]){fullDir = "Col: " + direccion["colonia"] + ", ";}
          if(direccion["calle"] && direccion["calle"] !== "Unnamed Road"){fullDir  += "Calle: " + direccion["calle"]+ ", "}
          if(direccion["calleNumero"]){fullDir += "No.: "+direccion["calleNumero"] + ", ";}
          if(direccion["municipio"]){fullDir += direccion["municipio"]+".";}

          deferred.resolve(direccion);
        } else {
          deferred.reject(utils.getError(
            'No se pudo localizar la dirección', "reverseGeocoderSuccess()")
          );
        }
      }
      else {
        deferred.reject(utils.getError(
          'Geocoder status: ' + status, "reverseGeocoderSuccess()")
        );
      }
    } // end of reverseGeocoderSuccess

    try{
      var geocoder  = new google.maps.Geocoder();
      geocoder.geocode({latLng: latLng}, reverseGeocoderSuccess);
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  function addressString(){
    return fullDir;
  }

  return {
    getLocationOnce:  getLocationOnce,
    reverseCoding:    reverseCoding,
    getFullAddress:   getFullAddress,
    addressString:    addressString
  };
});

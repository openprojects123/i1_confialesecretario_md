"use strict";

appServices.factory("botonBLESvc",function($rootScope, $q, utils){
  $rootScope.IS_BTN_CONNECTED = false;

  var device = null;

  var deviceNames = ["ITAG", "R2", "I ANTI-LOST"];

  var boton = {
    service: "ffe0",// Servicio
    char:    "ffe1" // Caracteristica
  };

  var battery = {
    service: "180f",// Servicio
    char:    "2a19" // Caracteristica
  };

  var autoConnect = true;

  /**
   * @description
   * Verifica si el BLE esta activado
   * @returns {Promise}
   *  Success: true
   *  Fail: false
   */
  function isEnabled(){
    var deferred = $q.defer();
    try{
      ble.isEnabled(
        function(){deferred.resolve(true);},//Cuando esta habilitado
        function(){deferred.reject(false);}//Cuando no esta habilitado
      );
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }
    return deferred.promise;
  }

  /**
   * @description
   * Intenta habilitar el BLE
   * @returns {Promise}
   *  Success: true
   *  Fail: (Object) parsedError
   */
  function enable() {
    var deferred = $q.defer();
    try{
      ble.enable(
        function(){deferred.resolve(true);},
        function(error){
          deferred.reject(utils.getError(error, "enable()"));
        });
    }catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  function startScan(btnId){
    var deferred = $q.defer();

    try{
      var found = false;
      /*if(!btnId.length){
        utils.putMsg.warn("botonBLESvc->startScan(): No se recibio el id del boton a conectar")
        //deferred.reject(utils.getError("botonBLESvc->startScan(): No se recibio el id del boton a conectar"));
        //return deferred.promise;
      }
      else*/
        utils.putMsg.info("Escaneando dispositivos BLE ("+btnId[0]+" || "+btnId[1]+")");


      ble.startScan([],//todos los servicios
        function(data){
        //console.log("------ "+JSON.stringify(data))
          if(typeof data.name != "undefined" && (deviceNames.indexOf(data.name.toUpperCase().trim())!=-1)){
            utils.putMsg.log("DEVICE FOUND: "+data.id);
            //utils.putMsg.success("FOUND DEVICE: "+JSON.stringify(data));
            //if(data.id === btnId || data.id === btnId[1]){
            found = true;
            ble.stopScan();
            deferred.resolve(data);
          }

        }, function(reason){
          deferred.reject(utils.getError(reason, "startScan()"));
        });

      setTimeout(ble.stopScan, 15*ONE_SECOND,
        function() {
          if(!found){
            deferred.reject(
              utils.getError("Se agotó el tiempo de escaneo", "stopScan()<-startScan()")
            );
          }
        },
        function(reason) {
          deferred.reject(reason);
        }
      );
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  /**
   * @description
   * Verifica si el boton BLE está vinculado
   * @returns {Promise}
   * Success: true
   * Fail: false
   */
  function isConnected() {
    var deferred = $q.defer();
    try{
      ble.isConnected(
        function(){deferred.resolve(true);},
        function(){deferred.reject(false);}
      );
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  /**
   * @description
   * Subscribe al telefono a notificaciones del boton BLE
   * Permite saber cuando se ha hecho click o doble click
   */
  function subscribeToButton(callbackFn){
    try{
      var lastClickTime = null;
      utils.putMsg.info("Intentando suscribirse a notifiaciones del botón");
      //subscribes to a Bluetooth characteristic (Button)
      ble.startNotification(device.id, boton.service, boton.char,
        function (data) {//Array buffer

          if(lastClickTime === null){
            utils.putMsg.info("Primer clic registrado");
            lastClickTime = utils.fecha().time;
            return;
          }

          var newClickTime = utils.fecha().time;
          //utils.putMsg.log("Segundo clic registrado: "+lastClickTime);
          //utils.putMsg.log("DIF: "+(newClickTime - lastClickTime));
          if((newClickTime - lastClickTime) > 1500){
            //utils.putMsg.log("Te tardaste!!!");
            lastClickTime = newClickTime;
            return;
          }

          lastClickTime = newClickTime;

          callbackFn(data);
        },
        function(error){
          utils.getError(error, "startNotification()<-subscribeToButton()");
        });
    }
    catch(e){
      utils.getError(e);
    }
  }


  function readBatteryLevel(){
    utils.putMsg.info("intentando leer el nivel de bateria...");
    try{
      //Read battery level
      ble.read(device.id, battery.service, battery.char,
        function(level){
          level = ""+String.fromCharCode.apply(null, new Uint8Array(level));
          level = level.charCodeAt(0);
          $rootScope.batteryLevel = level;

          utils.putMsg.success("Nivel de bateria: "+level+"%");
          utils.timeout(function(){
            $rootScope.$apply();
            subscribeToButton();
          }, 700);
        }, function(error){
          $rootScope.batteryLevel = "ERROR DE LECTURA";

          utils.getError(error, "readBatteryLevel()");

          subscribeToButton();
        });
    }
    catch(e){
      utils.getError(e);
    }
  }//<--readBatteryLevel()

  var contBLEConnect = 0;

  /**todo: arreglar ya que es recursiva, pero está mal diseñada
   * @description
   * Intenta conectar el boton y suscribirse a sus notificaciones
   * @param address
   *  (string) MAC/ID del botón
   * @param subscribeCallbackFn
   *  (function) funcion que se ejecutará cuando el boton se presione
   *  ver funcion interna subscribeToButton(callbackFn)
   * @param disconnectCallbackFn
   *  (function) Funcion que se ejecutará cada vez que el boton se desconecta
   * @returns {Promise}
   */
  function connect(address, subscribeCallbackFn, disconnectCallbackFn){
    var deferred = $q.defer();

    try{
      utils.putMsg.log("----- btLESvc: Intentando conectar con el BLE "+address+"...");
      contBLEConnect++;

      ble.connect(address, // device to connect to
        function(_device){

          device = _device;

          contBLEConnect = 0;

          autoConnect = true;
          //utils.putMsg.info("Name: "+_device.name);
          //utils.putMsg.info("ID: "+_device.id);

          //readBatteryLevel();
          subscribeToButton(subscribeCallbackFn);

          $rootScope.IS_BTN_CONNECTED = true;

          utils.timeout(function(){
            $rootScope.$apply();
            deferred.resolve(_device);
            utils.hideLoading();
          }, ONE_SECOND);
        },
        function(error){ //Callback cada que se desconecta
          $rootScope.IS_BTN_CONNECTED = false;

          disconnectCallbackFn(error);


          if(autoConnect && contBLEConnect < 5){ //Maximo 5 veces
            utils.showLoading();
            utils.putMsg.info(contBLEConnect+") Intentando Reconectar el BLE...");
            utils.timeout(function(){
              connect(address, subscribeCallbackFn, disconnectCallbackFn);
              deferred.reject(utils.getError(error), "connect()");
            }, 2*ONE_SECOND);

          }
          else{
            utils.putMsg.info("Ya no se intentará reconectar el BLE");
            autoConnect = false;
            contBLEConnect = 0;
            utils.timeout(function(){
              utils.hideLoading();
              $rootScope.$apply();
              deferred.reject(utils.getError(error, "connect()"));
            }, ONE_SECOND);
          }
        });
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  /**
   * @description
   * Cierra la conexion enre el Bluetooth y el boton BLE
   * @returns {Promise}
   */
  function closeConnection(){
    var deferred = $q.defer();
    try{
      ble.disconnect(device.id,
        function(){
          $rootScope.IS_BTN_CONNECTED = false;
          deferred.resolve(true);
        },
        function(error){
          deferred.reject(utils.getError(error, "closeConnection()"));
        });
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }
    return deferred.promise;
  }

  /**
   * @description
   * Helper para desconectar manualmente el botón
   * @returns {Promise}
   */
  function disconnect() {
    var deferred = $q.defer();

    try{
      autoConnect = false;

      //unsubscribes from any Bluetooth serial listener
      ble.stopNotification(device.id, boton.service, boton.char,
        function () {
          utils.putMsg.info("Notificaciones BLE detenidas OK");
          $rootScope.$apply();
          closeConnection()
          .then(function(){
            deferred.resolve(true);
          }, function(reason){
            deferred.reject(reason, "closeConnection()<-stopNotification()<-disconnect()");
          });
        },
        function(error){
          var error1 = utils.getError(error, "stopNotification()<-disconnect()");

          closeConnection()
          .then(function(){
            deferred.resolve(true);
          }, function(error2){
            deferred.reject(
              utils.getError(
                {
                  reason: error1.reason + " && " + error2.reason,
                  statusText: "No se pudieron detener las notificaciones ni desconectar el botón"
                },
                "closeConnection()<-stopNotification()<-disconnect()"
              )
            );
          });
        });
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  return {
    isEnabled:   isEnabled,
    enable:      enable,
    startScan:   startScan,
    isConnected: isConnected,
    connect:     connect,
    disconnect:  disconnect
  };
});

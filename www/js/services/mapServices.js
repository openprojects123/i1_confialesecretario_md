"use strict";

appServices.factory('mapSvc', function ($q, utils){
  var
    map = null,
    initialCenter;

  if(typeof google === "undefined"){
    utils.showAlertDialog("No se cargó la librería de mapas");
  }

  function isGoogleOk() {
    return (typeof google !== "undefined");
  }

  function init(containerId, initialLocation){
    var deferred = $q.defer();

    if(initialLocation === null || typeof initialLocation == 'undefined'){
      deferred.reject(utils.getError("No se recibió la ubicación inicial", "init()"));
      return deferred.promise;
    }

    var mapContainer = document.getElementById(containerId);
    if(!mapContainer){
      deferred.reject(utils.getError("No hay contenedor para el mapa", "init()"));
      return deferred.promise;
    }

    //Ya se habia inicializado
    if(map !== null){
      utils.putMsg.info("Ya existe el mapa");
      deferred.resolve(true);
    }

    if(!isGoogleOk()){
      deferred.reject(utils.getError("No se cargó la librería de mapas", "init()"));
      return deferred.promise;
    }


    initialCenter = initialLocation;
    setInitParams(mapContainer, initialLocation);
    deferred.resolve(true);

    return deferred.promise;
  }

  function setInitParams(mapContainer, initialLocation) {
    var mapTypeIds = [];
    for(var type in google.maps.MapTypeId) {
      mapTypeIds.push(google.maps.MapTypeId[type]);
    }
    mapTypeIds.push("OSM");

    map = new google.maps.Map(mapContainer, {
      center: initialLocation,
      zoom: 15,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeId: "OSM",
      mapTypeControl: false,
      streetViewControl: false,
      //zoomControl: false
    });

    map.mapTypes.set("OSM", new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
      },
      tileSize: new google.maps.Size(256, 256),
      name: "OpenStreetMap",
      maxZoom: 18
    }));

    //map.setCenter(initialLocation);
    map.infowindow = null;
    map.marker     = null;

    if(DEBUG_MODE){
      google.maps.event.addListener(map, "center_changed", function() {
        //utils.putMsg.info("on center_changed: "+JSON.stringify(map.getCenter()));
      });
    }

  }

  function getMap(){return map;}

  function getCenter() {
    return map.getCenter().latLng;
  }

  function setCenter(pos) {
    map.setCenter(pos);
  }

  function hasMap(){return map === null;}

  function reloadMap(){
    var deferred = $q.defer();

    if(!isGoogleOk()){
      deferred.reject(utils.getError("No se cargó Google Maps", "reloadMap()"));
      return deferred.promise;
    }

    utils.putMsg.info("Centrando mapa...");

    google.maps.event.trigger(map, 'resize');

    map.setCenter(initialCenter);
    map.setZoom(4);

    deferred.resolve(true);

    return deferred.promise;
  }


  function drawPolyline(coordinates){
    try{
      new google.maps.Polyline({
        path:          coordinates,
        strokeColor:   "#0000FF",
        strokeOpacity: 1.0,
        strokeWeight:  3,
        map:           map
      });
    }
    catch(e){
      utils.putMsg.log("drawPath(): "+e.message);
    }
  }

  //TODO codificar funcion drawPolygon()
  function drawPolygon(coordinates){

  }

  function setClickMapListener(callbackFn) {
    google.maps.event.addListener(map, 'click', function(e) {
      callbackFn(e.latLng);
    });
  }

  /**
   *
   * @param pos (Object) latLng; Requerido
   * @param config (Object) {...}; Opcional
   *  cleanMarkers: true(default)|false
   *  panToMarker:  true|false(default)
   *  animation: String <'drop'| 'bounce' | ''(default)>
   *  iconUrl:   String ''(default)
   *  clickCallback: funcion que se ejecutara al hacer clic
   *                 sobre el marcador creado
   */
  function placeMarker (pos, config) {
    if(!config)config = {};

    var
      replaceMarker = config.cleanMarkers  || true,
      panToMarker   = config.panToMarker   || false,
      animation     = config.animation     || "",
      iconUrl       = config.iconUrl       || "",
      clickCallback = config.clickCallback || null;


    try{
      if(!pos){pos = map.getCenter();}


      if((typeof map.marker != 'undefined' && map.marker !== null) && replaceMarker){
        map.marker.setMap(null);
      }

      if(animation == "drop")
        animation =  google.maps.Animation.DROP;
      else if(animation == "bounce")
        animation =  google.maps.Animation.BOUNCE;

      var markerConfig = {
        map: map,
        position: pos,
        animation: animation
      };

      if(iconUrl){
        markerConfig.icon = {
          url:    iconUrl,
          size:   new google.maps.Size(30, 49),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(15, 49)
        }
      }

      map.marker = new google.maps.Marker(markerConfig);

      if(clickCallback !== null)
        map.marker.addListener('click', clickCallback);


      if(panToMarker)
        map.panTo(pos);

    }
    catch(e){
      utils.putMsg.warn("try mapSvc->placeMarker(): "+e.message);
    }
  }

  function deleteMarker(){
    map.marker.setMap(null);
    map.marker = null;
  }

  function placeInfoWindow(content){
    try{
      if(map.infowindow !== null)
        map.infowindow.close(map);

      map.infowindow = new google.maps.InfoWindow();

      map.infowindow.setContent(content);
      map.infowindow.open(map, map.marker);
      //map.panTo(getCenter());
    }
    catch(e){
      utils.putMsg.warn("try mapSvc->placeInfoWindow(): "+e.message);
    }
  }


  return {
    init:                init,
    getMap:              getMap,
    getCenter:           getCenter,
    setCenter:           setCenter,
    hasMap:              hasMap,
    reloadMap:           reloadMap,
    //drawPolyline:        drawPolyline,
    placeMarker:         placeMarker,
    deleteMarker:       deleteMarker,
    placeInfoWindow:     placeInfoWindow,
    setClickMapListener: setClickMapListener
  };
});

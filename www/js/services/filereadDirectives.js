"use strict";

appServices.directive("fileread", function ($rootScope, fileMgrSvc, utils) {

  return {
    scope: {
      fileread: "="
    },
    link: function (scope, element, attributes) {

      element.bind("change", function (changeEvent) {
        if(element[0].value == ''){
          utils.putMsg.error("El valor del input:file está vacío");
          return;
        }

        var reader = new FileReader();
        reader.onload = function (loadEvent) {
          var f = changeEvent.target.files[0];
          var imgExt = ['bmp', 'png', 'jpg', 'jpeg', 'gif'];
          var fileExtension = f.name.substring(f.name.lastIndexOf('.')+1).toLocaleLowerCase();

          var found = false;
          imgExt.some(function(ext){
            if(ext === fileExtension)
              return found = true;
          });

          var file = {
            nombre:       f.name,
            size:         f.size,
            isImage:      found,
            lastModified: f.lastModified,
            uri:          loadEvent.target.result
          };

          //Reseteo el input para que, si inmediatamente vuelve a elegir el mismo archivo
          //pueda validar que ya existe, de no hacerlo no se genera el evento onchange
          element[0].value = '';

          if(typeof fileMgrSvc.isFileAlreadyInList === 'function'){
            if(fileMgrSvc.isFileAlreadyInList(file)){
              //$rootScope.$broadcast('file-reading-already-exist');
              return;
            }
          }
          else{
            utils.putMsg.error(
              "La función fileMgrSvc.isFileAlreadyInList() no está definida"
            );
            return;
          }


          scope.$apply(function () {
            scope.fileread = loadEvent.target.result;
          });

          $rootScope.$broadcast('file-reading-finished', file);
        };//<-- reader.onload

        //Si el usuario no canceló la búsqueda...
        if(changeEvent.target.files[0]){
          $rootScope.$broadcast('file-reading-started');
          reader.readAsDataURL(changeEvent.target.files[0]);
        }
      });//<-- element.bind
    }//<-- link
  };
});

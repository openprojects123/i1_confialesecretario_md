"use strict";

appServices.factory("trackingSvc", function ($rootScope, $q, gpsLocationSvc, utils) {
  $rootScope.IS_TRACKING = false;

  var
    lastLocationEvent = "",//fecha+hora de la ultima obtencion (usada en IOS)
    mustStopLocations = true;

  /**
   * @description
   * Helper para obtener la ubicacion N veces hasta que mustStopLocations === true,
   * @param period
   * (integer) cada N milisegundos que se intentará obtener la localización
   * @param successFn
   * (function) Función que se ejecutará cada vez que se resuelva la localización
   * @param failCallbackFn
   * (function) Función que se ejecutará si no se resuelve la localización
   */
  function locationLoop(period, successFn, failCallbackFn){

    if(!period || typeof period==='undefined' || period < 30*ONE_SECOND){
      utils.showAlertDialog(
        "locationLoop(): El periodo de ejecucion debe ser >= 30 segundos!, pero es de "+
        period
      );
      return;
    }

    if(mustStopLocations)return;

    /*/fixme:******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
    utils.putMsg.error("*** locationLoop() en modo DEBUG!");
    utils.showLoading();
    gpsLocationSvc.getLocationOnce().then(function (pos) {
      utils.hideLoading();
      successFn(pos);
    });
    utils.timeout(function () {
      locationLoop(period, successFn, failCallbackFn);
    },period);
    return;
    //************************************************************************/

    utils.showLoading();
    gpsLocationSvc.getLocationOnce()
    .then(function(location){
        if(mustStopLocations)return;//<-por si aun no se resolvia, una vez presionado el btn finalizar
        $rootScope.IS_TRACKING = true;

        var Hoy  = utils.fecha().hoy;
        var hora = utils.fecha().currtime;

        if(lastLocationEvent !== Hoy+hora){
          utils.putMsg.info("Posición: {"+location.lat + "; "+location.lng+"; "+location.speed+"}");
          successFn(location);
        }
        else{
          utils.putMsg.warn("Misma ubicación, ignorando...");
        }

        utils.timeout(function(){$rootScope.$apply();}, 300);
        lastLocationEvent = Hoy+hora;
      },
      function(error){
        failCallbackFn(error);
      }).finally(function () {
        utils.hideLoading();
      });

    //Pase lo que pase, si !mustStopLocations entonces se vuelve a jecutar
    utils.timeout(function () {
      locationLoop(period, successFn, failCallbackFn);
    },period);
  }//<--locationLoop()

  /**
   * @description
   * Inicializa el loop para obtener la geolocalizacion
   * En iOS utiliza el helper locationLoop(...)
   * En Android utiliza el plugin que controla el periodo de consulta
   * internamente, por lo que por ahora no es posible cambiarlo dinamicamente
   */
  function startTrackingLoop(period, sueccesCallbackFn, failCallbackFn){
    try{
      console.log("Tracking iniciado!")
      /*/fixme:******************************************************************
      //*                             PARA DEBUG                               *
      //************************************************************************
       utils.putMsg.error("*** startTrackingLoop() en modo DEBUG!");
      mustStopLocations = false;
      if($rootScope.PLATFORM.isAndroid())
        $rootScope.IS_TRACKING = true;

      locationLoop(period, sueccesCallbackFn, failCallbackFn);
      return;
      //************************************************************************/
      if($rootScope.PLATFORM.isIOS() || $rootScope.PLATFORM.isIPad()){
        mustStopLocations = false;
        locationLoop(period, sueccesCallbackFn, failCallbackFn);
      }
      else if($rootScope.PLATFORM.isAndroid()){
        window.pluginTest.start(function(location){
          $rootScope.IS_TRACKING = true;
          sueccesCallbackFn(location);
        }, function(error){
          failCallbackFn(utils.getError(error, "startTrackingLoop()"));
        });
      }
      else{
        console.warn("Plataforma no soportada!")
      }
    }
    catch(e){
      failCallbackFn(utils.getError(e));
    }
  }

  /**
   * @description
   * Helper Se ejecuta una vez que se ha podido detener el tracking
   * @param resp
   */
  function onStopTrackingLoop(resp) {
    $rootScope.IS_TRACKING = false;
    utils.putMsg.info("Tracking Finalizado: "+resp);
    utils.timeout(function(){$rootScope.$apply();}, 300);
  }

  /**
   * @description
   * Intenta detener el loop de geolocalizacion
   * @returns {Promise}
   * Success: true
   * Fail: (string) reason<ios>|(Object) parsedError <android>
   */
  function stopTrackingLoop(){
    var deferred = $q.defer();

    mustStopLocations = true;//detener en iOS

    /*/fixme*******************************************************************
    //*                             PARA DEBUG                               *
    //************************************************************************
     utils.putMsg.error("*** stopTrackingLoop() en modo DEBUG!");
    utils.timeout(function () {
     utils.putMsg.success("Fin stopTrackingLoop() en modo DEBUG!");
      onStopTrackingLoop("OK");
      deferred.resolve(true);
    }, ONE_SECOND);
    return deferred.promise;
    //************************************************************************/

    try{
      if($rootScope.PLATFORM.isIOS()){
        onStopTrackingLoop("OK");
        deferred.resolve(true);
      }
      else{
        //fixme: será mejor llamar a locationLoop()???
        //Ya que la funcion startTrackingLoop en android NO mando a llamar a
        // locationLoop (que internamente llama a getLocationOnce()!!!)
        // sino directamente a window.pluginTest.start().
        window.pluginTest.stop(function(resp){
          utils.putMsg.info("Geolocalización Finalizada: "+JSON.stringify(resp));
          onStopTrackingLoop("OK");
          deferred.resolve(location);
        }, function(reason){
          deferred.resolve(utils.getError(reason));
        });
      }
    }
    catch(e){
      deferred.reject(utils.getError(e));
    }

    return deferred.promise;
  }

  return {
    startTrackingLoop: startTrackingLoop,
    stopTrackingLoop:  stopTrackingLoop
  };
});

"use strict";

appServices.factory('fileMgrSvc',
  function($rootScope, $q, $window, $cordovaFileTransfer, utils){
    $rootScope.fileList = [];

    /**
     * Funcion usada junto con el plugin file-chooser
     * @param uri
     * @returns {*|promise}
     */
    function addFileToList(uri){
      var deferred = $q.defer();
      try{
        var archivo = {
          id:       $rootScope.fileList.length,
          nombre:   uri.substring(uri.lastIndexOf("/")+1, uri.length),
          uri:      uri,
          total:    0,    //porcentje subido
          subidoOk: false //No se subio correctamente
        };

        utils.putMsg.info("Archivo elegido: "+archivo.nombre);

        if($rootScope.fileList.length){
          var duplicado = false;
          var idxDuplicado = -1;
          $rootScope.fileList.some(function(data, idx){
            if(archivo.uri === data.uri){
              idxDuplicado = idx;
              return (duplicado = true);
            }
          });
          if(duplicado){
            deferred.reject(
              utils.extractError({
                statusText: "Archivo duplicado",
                reason: "El archivo '"+$rootScope.fileList[idxDuplicado].nombre+
                "' ya existe en la lista"
              })
            );
            return deferred.promise;
          }
        }
        $rootScope.fileList.push(archivo);
        deferred.resolve($rootScope.fileList);
      }
      catch(e){
        deferred.reject(utils.getError(e));
      }
      return deferred.promise;
    }

    /**
     * Funcion usada junto con el plugin mfilechooser
     * @returns {*|promise}
     */
    function chooseFile(){
      var deferred = $q.defer();
      try{
        //utils.putMsg.info("Plataforma: " + ionic.Platform.platform());

        if(ionic.Platform.isAndroid()){
          window.plugins.mfilechooser.open([], function (uri){
            addFileToList(uri)
              .then(function(list){
                deferred.resolve(list);
              }, function(error){
                deferred.reject(utils.getError(error, "chooseFile()"));
              });
          }, function (error) {
            deferred.reject(utils.getError(error, "chooseFile()"));
          });
        }
      }
      catch(e){
        deferred.reject(utils.getError(e));
      }
      return deferred.promise;
    }//<--chooseFile()

    /**
     * evento generado en directiva: filereadDirective
     */
    $rootScope.$on('file-reading-started', function () {
      utils.showLoading();
    });

    /**
     * evento generado en directiva: filereadDirective
     */
    $rootScope.$on('file-reading-finished', function (e, file) {
      file.id         = $rootScope.fileList.length;
      file.total      = 0;     //porcentje subido
      file.subidoOk   = false; //No se subio correctamente
      file.errorObj   = {};
      file.codedName  = '';//Como lo renombró la API
      file.typeNumber = '';//Como lo clasifico la API

      /*El objeto final contiene ademas estos atributos:
       nombre:       string,
       size:         number,
       isImage:      bool,
       lastModified: string?,
       uri:          string
       */


      $rootScope.fileList.push(file);

      utils.showToast(
        "Archivo <span style='color: #32d3fb'>"+file.nombre+"</span> agregado"
      );
      utils.hideLoading();
    });

    /**
     * Funcion usada por directiva: filereadDirective
     * @param file
     * @returns {boolean}
     */
    function isFileAlreadyInList(file) {
      if($rootScope.fileList.length){
        var duplicado = false;
        $rootScope.fileList.some(function(data){
          if(file.nombre      === data.nombre &&
            file.size         === data.size &&
            file.lastModified === data.lastModified){
            return (duplicado = true);
          }
        });
        if(duplicado){
          utils.showToast(
            "Archivo <span style='color: #ff6209'>"+file.nombre+"</span> ya existe"
          );
          utils.hideLoading();
          return true;
        }
      }
      return false;
    }

    function removeFile(id){
      var deferred = $q.defer();
      try{
        if(!$rootScope.fileList.length){
          deferred.reject(
            utils.extractError({
              statusText:"Lista vacía",
              reason:"La lista de archivos está vacía"
            })
          );
        }
        else if(id >= 0){
          $rootScope.fileList.splice(id, 1);
          deferred.resolve(true);
        }
        else{
          deferred.reject(
            utils.extractError({
              statusText:"Archivo no encontrado",
              reason:"No se encontró el archivo solicitado"
            })
          );
        }
      }
      catch(e){
        deferred.reject(utils.getError(e));
      }

      return deferred.promise;
    }

    function uploadFile(file){
      var deferred = $q.defer();
      try{
        var options = {
          //Este es el nombre (campo) que espera recibir el servidor: $_FILES["fileToUpload"]["tmp_name"]
          fileKey: "fileUpload",
          fileName: file.nombre,
          chunkMode: "false",
          mimeType: "image/png"
        };


        //(url_servidor, url_recurso_local, opciones)
        $cordovaFileTransfer.upload(API_URL + "common/multimedia/uploadFiles/prefix/m.CS.", file.uri, options)
          .then(function(API){
            console.log(JSON.stringify(API))
            API = JSON.parse(API.response);
            if(API.result){
              deferred.resolve(API.data.fileInfo);
            }
            else{
              //La API no devuelve mensaje de error
              deferred.reject(utils.getError("Ocurrio un error desconocido", "uploadFile(API)"));
            }
          }, function(error){
            console.error(JSON.stringify(error))
            deferred.reject(utils.getError(error, "uploadFile()"));
          }, function (progress) {
            deferred.notify(progress);
          });
      }
      catch(e){
        console.error(JSON.stringify(e))
        deferred.reject(utils.getError(e));
      }//try-catch
      return deferred.promise;
    }//<--uploadFile()

    function multiUpload() {
      var deferred = $q.defer();
      var totFiles = $rootScope.fileList.length;
      if(!totFiles){
        deferred.reject(
          utils.extractError({
            statusText: "Lista vacía",
            reason: "No se encontraron archivos para subir"
          })
        );
        return deferred.promise;
      }

      var totResolves   = 0;
      var numUploadedOk = 0;
      $rootScope.fileList.forEach(function (file, idx) {
        if(!file.subidoOk){
          uploadHelper(file, idx)
          .then(function (_file) {
            if(_file.resp.estatus === "ok"){
              $rootScope.fileList[_file.id].subidoOk   = true;
              $rootScope.fileList[_file.id].codedName  = _file.resp.codedName;
              $rootScope.fileList[_file.id].typeNumber = _file.resp.typeNumber;
              numUploadedOk++;
            }
            else{
              $rootScope.fileList[_file.id].total    = 0;
              $rootScope.fileList[_file.id].errorObj = err.resp;
            }

            totResolves++;
            if(totResolves === totFiles)
              deferred.resolve(numUploadedOk)
          }, function (err) {
            //utils.putMsg.log(JSON.stringify(err));
            if($rootScope.fileList[err.id]){
              $rootScope.fileList[err.id].total    = 0;
              $rootScope.fileList[err.id].errorObj = err.error;
            }

            totResolves++;
            if(totResolves === totFiles)
              deferred.resolve(numUploadedOk)
          }, function (p) {//Progreso
            $rootScope.fileList[p.id].total = p.progress;
          });
        }
      });

      return deferred.promise;
    }

    function uploadHelper(file, idx) {
      var deferred = $q.defer();

      uploadFile(file)
      .then(function (resp) {
        //resp = JSON.parse(resp.response);
        deferred.resolve({id: idx, resp: resp});
      }, function (error) {
        deferred.reject({id: idx, error: error});
      }, function (progress) {
        deferred.notify({
          id: idx,
          progress: (progress.loaded / progress.total) * 100
        });
      });

      return deferred.promise;
    }


    return{
      chooseFile:           chooseFile,
      isFileAlreadyInList:  isFileAlreadyInList,
      removeFile:           removeFile,
      multiUpload:          multiUpload
    };
  });

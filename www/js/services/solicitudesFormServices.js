"use strict";

appServices.factory("solicitudesFormSvc", function($rootScope, $q, utils){
  var
    estados = [],
    hechos  = [];

  var attempsGetCatCategoria = 0;
  function getCatCategoria(iddenuncia){
    var deferred = $q.defer();

    if(hechos.length){
      utils.putMsg.info( "Catálogo Hechos cargado desde Memoria");
      deferred.resolve(hechos);
      return deferred.promise;
    }

    function qry() {
      utils.myHttpGet(API_URL + "common/catHechos/denunciaTipo/" + iddenuncia || 1) //TODAS SON DENUNCIA TIPO 1
        .then(function (API) {
            if(API.result){
              hechos = API.data;
              utils.putMsg.info( "Catálogo categorias cargado desde DB");
              deferred.resolve(hechos);
            }
            else{
              deferred.reject(utils.getError(API, "getCatCategoria(API)"));
            }
          },
          function (error) {
            if(++attempsGetCatCategoria < HTTP_ATTEMPTS){
              utils.putMsg.info(
                "Intento "+(attempsGetCatCategoria+1)+"/"+HTTP_ATTEMPTS+
                " de obtener el catalogo de Categorias...");
              utils.timeout(qry, HTTP_TIMER);
            }
            else{
              deferred.reject(utils.getError(error, "getCatCategoria()"));
            }
          });
    }
    qry();

    return deferred.promise;
  }

  var attempsGetCatSubcategoria = 0;
  function getCatSubcategorias(idhechos){
    var deferred = $q.defer();

    function qry() {
      utils.myHttpGet(API_URL + "common/catDelitos/hecho/" + idhechos)
      .then(function (API) {
        if(API.result){
          utils.putMsg.info( "Catálogo Subcategorias cargado desde DB");
          deferred.resolve(API.data);
        }
        else{
          deferred.reject(utils.getError(API, "getCatSubcategorias(API)"));
        }
      }, function (error) {
        if(++attempsGetCatSubcategoria < HTTP_ATTEMPTS){
          utils.putMsg.info(
            "Intento "+(attempsGetCatSubcategoria+1)+"/"+HTTP_ATTEMPTS+
            " de obtener el catalogo de Subcategorias...");
          utils.timeout(qry, HTTP_TIMER);
        }
        else{
          deferred.reject(error, "getCatSubcategorias()");
        }
      });
    }
    qry();

    return deferred.promise;
  }

  var attempsGetCatEstados = 0;
  function getCatEstados(){
    var deferred = $q.defer();

    if(estados.length){
      utils.putMsg.info("Catálogo Estados cargado desde memoria");
      deferred.resolve(estados);
      return deferred.promise;
    }

    function qry() {
      utils.myHttpGet(API_URL + "common/catEstados")
        .then(function(API){
          if(API.result){
            estados = API.data;
            utils.putMsg.info("Catálogo Estados cargado desde la BD");
            deferred.resolve(estados);
          }
          else{
            deferred.reject(utils.getError(API, "getCatEstados(API)"));
          }
        }, function(error){
          if(++attempsGetCatEstados < HTTP_ATTEMPTS){
            utils.putMsg.info(
              "Intento "+(attempsGetCatEstados+1)+"/"+HTTP_ATTEMPTS+
              " de obtener el catalogo de Estados...");
            utils.timeout(qry, HTTP_TIMER);
          }
          else{
            deferred.reject(error, "getCatEstados()");
          }
        });
    }
    qry();

    return deferred.promise;
  }

  var attempsGetCatMunicipios = 0;
  function getCatMunicipios(idEstado){
    var deferred = $q.defer();

    function qry() {
      utils.myHttpGet(API_URL +"common/catMunicipios/estado/"+idEstado)
        .then(function (API) {
          if(API.result){
            utils.putMsg.info( "Catálogo Municipios cargado desde DB");
            deferred.resolve(API.data);
          }
          else{
            deferred.reject(utils.getError(API, "getCatMunicipios(API)"));
          }
        }, function(error){
          if(++attempsGetCatMunicipios < HTTP_ATTEMPTS){
            utils.putMsg.info(
              "Intento "+(attempsGetCatMunicipios+1)+"/"+HTTP_ATTEMPTS+
              " de obtener el catalogo de Municipios...");
            utils.timeout(qry, HTTP_TIMER);
          }
          else{
            deferred.reject(error, "getCatMunicipios()");
          }
        });
    }
    qry();

    return deferred.promise;
  }


  function saveDenuncia(formData){
    var deferred = $q.defer();
    formData.userId = $rootScope.SESSION_ATTRS.getUserId();

    if($rootScope.MODULES.SESSION.USER_AUTH && !$rootScope.SESSION_ATTRS.isAuthenticated()){
      deferred.reject(
        utils.getError(
          "No fue posible determinar su ID de usuario, será necesario volver a iniciar la sesión.",
          "saveDenuncia()"
        )
      );
      return deferred.promise;
    }

     /*/fixme*****************************************************************************
     //*                               FOR TESTING                                      *
     //**********************************************************************************
     utils.putMsg.error("saveDenuncia() en modo TESTING")
     deferred.resolve({idTicket: 174});
     return deferred.promise;
     //**********************************************************************************/

    utils.myHttpPost(
      API_URL + "common/denuncia/new",
      formData
    ).then(function (API) {
        if(API.result)
          deferred.resolve(API.data);
        else{
          deferred.reject(utils.getError(API, "saveDenuncia(API)"));
        }
      },
      function(error){
        deferred.reject(error, "saveDenuncia()");
      });

    return deferred.promise;
  }

  function saveRelacionTicketUsuario(idTicket){
    var deferred = $q.defer();

    /*/fixme******************************************************************************
     //*                               FOR TESTING                                      *
     //**********************************************************************************
     utils.putMsg.error("saveRelacionTicketUsuario() en modo TESTING")
     deferred.resolve(true);
     return deferred.promise;
     //**********************************************************************************/

    /*var formData={
      idTicket:  idTicket,
      idUsuario: $rootScope.MODULES.SESSION.USER_AUTH ? $rootScope.SESSION_ATTRS.getUserId() : 2
    };*/

    utils.myHttpPost(
      API_URL + "common/denuncia/ticket/"+idTicket+"/usuario/"+
      //todo: por que el usuario id: 2???
      ($rootScope.MODULES.SESSION.USER_AUTH ? $rootScope.SESSION_ATTRS.getUserId() : 2)
    ).then(function (API) {
      if(API.result)
        deferred.resolve(API.data);
      else{
        deferred.reject(utils.getError(API, "saveRelacionTicketUsuario(API)"));
      }
    }, function(error){
      deferred.reject(error, "saveRelacionTicketUsuario()");
    });
    return deferred.promise;
  }


  /**
   * valida un ticket, usado en la seccion Multimedia
   * @param formData
   * @returns {Promise}
   */
  function validateTicket(formData){
    var deferred = $q.defer();
    var myUrl = API_URL +
      "common/tipoDenuncia/"+ formData.iddenuncia+
      "/idDenuncia/"+ formData.idticket+
      "/hecho/"+ formData.idhecho +
      "/delito/"+ formData.iddelito+"/validar";

    utils.myHttpGet(myUrl)
    .then(function (API) {
      if(API.result){
        deferred.resolve(API.data);
      }
      else{
        deferred.reject(utils.getError(API, "validateTicket(API)"));
      }
    },
    function(error){
      deferred.reject(error, "validateTicket()");
    });
    return deferred.promise;
  }

  return{
    getCatEstados:             getCatEstados,
    getCatMunicipios:          getCatMunicipios,
    getCatCategoria:           getCatCategoria,
    getCatSubcategorias:       getCatSubcategorias,
    saveDenuncia:              saveDenuncia,
    saveRelacionTicketUsuario: saveRelacionTicketUsuario,
    validateTicket:            validateTicket
    //insertLocalRecord:         insertLocalRecord,
  }
});

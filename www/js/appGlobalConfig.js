"use strict";

var DEBUG_MODE = false; //usada en controladores
var DEV_MODE   = true;

var API = "MobileApps/"+(DEV_MODE? "API/": "")+
          "web/"+(DEV_MODE? "app_dev.php/": "")+"confialeSecretario/";

var servers = [
  "http://eonproduccion.net/",              //Desarrollo
  "http://cuartel.eonproduccion.net:39480/",//Produccion
];
var API_URL = servers[DEV_MODE ? 0 : 1 ] + API;

if(DEBUG_MODE){
  console.warn("APP: La aplicacion está en Modo Desarrollo");
  console.info("WS: "+ API_URL);
}

var
  ONE_SECOND     = 1000,
  ONE_MINUTE     = 1000*60,
  HTTP_TIMER     = 5*ONE_SECOND,
  ALERT_TIMER    = ONE_MINUTE,
  HTTP_ATTEMPTS  = 3,
  LEVEL_DEBUGGER = 'info';//{info, success, log, warn} //errores siempre se guardan

var APP_MODULES = {
  ALERTS:         true,
  BG_MODE:        true,
  BLUETOOTH:      true,
  ENCUESTA: {
    PREGUNTAS: true,
    VEHICULOS: true,
    SUJETOS:   true,
    OBJETOS:   true,
    MM_FILES:  true //Archivos multimedia
  },
  SESSION: {
    USER_AUTH:    true, //Permite usar la app o x caracteristica solo si se esta logueado?
    REGISTER:     true, //Permite registrarse
    CONFIRM_REG:  true, //Permite confirmar su registro?
    PASS_RECOVER: true,
    PASS_CHANGE:  true
  },
  TRACKING:         true,
  UPDATES:          true,
  UI_PANIC_BUTTON:  true,  //Boton(es) de panico en UI (pantalla)?
};

"use strict";

angular.module('routes', [])

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('inicio', {
        url: '/inicio',
        templateUrl: 'templates/inicio.html',
        controller: 'inicioCtrl'
      })
      .state('solicitudForm', {
        url: '/solicitudForm',
        templateUrl: 'templates/solicitudes/solicitudForm.html',
        controller: 'solicitudFormCtrl'
      })
      .state('solicitudSurvey', {
        params: {denuncia: null},
        url: '/solicitudSurvey',
        templateUrl: 'templates/solicitudes/solicitudSurvey.html',
        controller: 'solicitudSurveyCtrl'
      })
      .state('solicitudMultimedia', {
        url: '/solicitudMultimedia',
        templateUrl: 'templates/solicitudes/solicitudMultimedia.html',
        controller: 'solicitudMultimediaCtrl'
      })
      .state('solicitudConsulta', {
        url: '/solicitudConsulta',
        templateUrl: 'templates/solicitudes/solicitudConsulta.html',
        controller: 'solicitudConsultaCtrl'
      })
      .state('misSolicitudes', {
        url: '/misSolicitudes',
        templateUrl: 'templates/solicitudes/misSolicitudes.html',
        controller: 'misSolicitudesCtrl'
      })
      .state('sesion', {
        url: '/sesion',
        templateUrl: 'templates/sesion/sesion.html',
        controller: 'sesionCtrl'
      })
      .state('perfil', {
        url: '/perfil',
        templateUrl: 'templates/sesion/perfil.html',
        controller: 'perfilCtrl'
      })
      .state('bluetooth', {
        url: '/bluetooth',
        templateUrl: 'templates/bluetooth.html',
        controller: 'bluetoothCtrl'
      })
      .state('contactanos', {
        url: '/contactanos',
        templateUrl: 'templates/contactanos.html',
        controller: 'contactanosCtrl'
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/inicio');

  });

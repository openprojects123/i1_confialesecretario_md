//Acerca de este controlador...
"use strict";
appControllers.controller("contactanosCtrl", function($scope, utils){
  utils.putMsg.info("--contactanosCtrl");

    $scope.contactForm = {
      userNombre:    null,
      userEmail:     null,
      userComentario:null
    };


    $scope.sendComentarios = function(form){
      utils.showLoading();
      utils.myHttpPost(
        API_URL + "common/acercaDe/comentario",
        $scope.contactForm
      ).then(function(API){
        if(API.result){
          utils.putMsg.success(API.data);
          utils.showAlertDialog(API.data,
            {title: "Comentarios recibidos"}
          );
          $scope.contactForm = {userNombre:'', userEmail:'', userComentario:''};
          form.$setPristine();
          form.$setUntouched();
        }
        else{
          var e = utils.getError(API, "sendComentarios(API)");
          utils.showAlertDialog(
            "No se pudieron enviar los comentarios ya que: "+
            (DEBUG_MODE? "<hr>"+e.fullError: e.reason)+
            ", inténtelo nuevamente más tarde"
          )
        }
      }, function(error){
        var e = utils.getError(error, "sendComentarios()");
        utils.showAlertDialog(
          "No se pudieron enviar los comentarios ya que: "+
          (DEBUG_MODE? "<hr>"+e.fullError: e.reason)
          +", inténtelo nuevamente más tarde."
        );
      }).finally(function () {
        utils.hideLoading();
      });
    };//<--sendComentarios()
  });

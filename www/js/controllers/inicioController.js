"use strict";

appControllers.controller('inicioCtrl',
function ($rootScope, $scope, $sce, emergencySvc, utils) {
  utils.putMsg.info("--inicioCtrl");

  if($rootScope.DEBUG_MODE){
    utils.showToast("Aplicación en 'Modo Desarrollo'", 5*ONE_SECOND);
  }

  //Botones de Redes sociales
  $scope.buttons = [
    {
      buttonClass:"button-positive",
      buttonStyle: "",
      url: "http://guerrero.gob.mx/",
      iconClass: "",
      title: "guerrero.gob.mx",
      titleStyle:""
    },{
      buttonClass:"",
      buttonStyle: "background-color: #3a5795; color: white",
      url: "https://www.facebook.com/AstudilloFloresHector",
      iconClass: "ion-social-facebook",
      title: "Gobierno del Estado",
      titleStyle:""
    },{
      buttonClass:"button-calm",
      buttonStyle: "background-color: #55acee",
      url: "https://twitter.com/HectorAstudillo",
      iconClass: "ion-social-twitter",
      title: "Gobierno del Estado",
      titleStyle:""
    },{
      buttonClass:"button-positive",
      buttonStyle: "",
      url: "http://www.seguridadgro.gob.mx/inicio/",
      iconClass: "  ",
      title: "SSP Guerrero",
      titleStyle:""
    }
  ];

  $scope.goToDenunciaAnonima = function(){
    if(!$rootScope.hechosPosition)
      utils.showLoading();

    utils.timeout(function(){
      $rootScope.goToState("solicitudForm");
    }, 600);
  };

  $scope.openInappBrowser = function(url){
    window.open(url, '_blank', 'location=yes');
  };

  if($rootScope.MODULES.TRACKING){
    $scope.confirmEmergency = function(){
      //Si ya estaba haciendo tracking... que deje de hacerlo.

      if($rootScope.IS_TRACKING){
        emergencySvc.stopEmergencyAndTracking();
        return;
      }

      if($rootScope.SESSION_ATTRS.isAuthenticated()){
        utils.showConfirmDialog(
          "Sólo debes usar este botón en caso de una emergencia <b>REAL</b>",{
            ok: "Es real"
          }
        ).then(function(){
          emergencySvc.startEmergencyAndTracking();
        });
      }else{
        $rootScope.goToState("sesion");
      }
    };
  }

  $scope.blog = {
    titulo:   $sce.trustAsHtml('<b style="color: #F4A01E">Bienvenidos </b>'),
    logo:     "img/mandatario.png",
    mensaje:  $sce.trustAsHtml("<p style='color: #AA00B3'>Ponemos a tu disposición esta nueva aplicación.</p>")
  };

  (function loadBlog() {
    utils.myHttpGet(API_URL + "public/blog")
      .then(function(data){
        if(data.result){
          data = data.data;
          $scope.blog = {
            titulo:  $sce.trustAsHtml(data.titulo) || $sce.trustAsHtml("<b style='color: #F4A01E'>Sin Título :( </b>"),
            logo:    data.logo,
            mensaje: $sce.trustAsHtml(data.mensaje)
          };
        }
        else{
          utils.putMsg.log(
            "(API ERROR) No se obtuvo el blog ya que: "+
            utils.extractError(API).fullError
          );
        }
      }, function(error){
        utils.putMsg.log(
          "(ERROR) No se obtuvo el blog ya que: "+
          utils.extractError(error).fullError
        );
      });
  })();

  document.addEventListener("online", function(){
    loadBlog();
  }, false);//ON-ONLINE

});

"use strict";

appControllers

  .controller("consoleCtrl",
    function($rootScope, $ionicScrollDelegate, utils) {
      utils.putMsg.info("--consoleCtrl");

      var isConsoleOpen = false;//Oculta
      $rootScope.toggleConsoleLog = function () {
        if(isConsoleOpen)
          document.getElementById("consoleLog").style.visibility = "hidden";
        else{
          document.getElementById("consoleLog").style.visibility = "visible";
          $ionicScrollDelegate.$getByHandle('consoleScroll').scrollBottom();
        }
        isConsoleOpen = !isConsoleOpen;
      };

      $rootScope.clearLogConsole = function () {
        utils.showConfirmDialog("¿Limpiar la consola?")
          .then(function () {
            utils.clearConsole();
            $rootScope.toggleConsoleLog();
          })
      };
    })

  .controller("DialogController", ["$scope", "$mdDialog", "displayOption",
    function ($scope, $mdDialog, displayOption) {
      $scope.displayOption = displayOption;
      $scope.cancel = function () {
        $mdDialog.cancel();
      };
      $scope.ok = function () {
        $mdDialog.hide();
      };
    }])

  .controller("toastController", ["$scope", "displayOption",
    function ($scope, displayOption) {
      $scope.displayOption = displayOption;
    }]);

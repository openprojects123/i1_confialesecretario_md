"use strict";

//Se incluye userSvc para que se inicialicen internamente los atributos: SESSION_ATTRS
// y se validen donde sea necesario... nada más
appControllers.controller('diagnosticsCtrl', function($rootScope, $ionicPlatform, gpsLocationSvc, diagnosticSvc, userSvc, utils){
  utils.putMsg.info("--diagnosticsCtrl");

  $ionicPlatform.ready(function(){

    //Cuando está en modo web
    if(!$rootScope.PLATFORM.device().uuid && !$rootScope.SESSION_ATTRS.isAuthenticated()){
      utils.putMsg.info("Desde plataforma web!");
      utils.showConfirmDialog(
        "¿No has iniciado sesión, deseas iniciar ahora?"
      ).then(function () {
        $rootScope.goToState("sesion");
      })
    }

    document.addEventListener("deviceready", function () {
      checkInternet();

      function checkInternet() {
        utils.putMsg.info("Ejcutando checkInternet()");
        if(!diagnosticSvc.isOnline()){
          utils.putMsg.info("Sin conexión a internet");
          diagnosticSvc.isWifiEnabled()
            .then(function(enabled){
              if(!enabled){
                if(ionic.Platform.isAndroid()){
                  utils.showConfirmDialog(
                    "El WiFi no está habilitado, ¿desea habilitarlo ahora?"
                  ).then(function(){
                    diagnosticSvc.switchToWifiSettings();
                    checkLocationEnabled();
                  }, function () {
                    utils.showAlertDialog(
                      "La aplicación requiere de WiFi o datos para funcionar correctamente."
                    ).then(function () {
                      checkLocationEnabled();
                    });
                  });
                }
                else{//Es iOS
                  utils.showToast("El WiFi no está habilitado", 3*ONE_SECOND);
                  checkLocationEnabled();
                }
              }
            }, function(error){
              utils.showAlertDialog(
                "No se pudo determinar el estado del Wifi ya que: "+
                (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
              ).then(function () {
                checkLocationEnabled();
              });
            });
        }
        else{
          checkLocationEnabled();
        }
      }//<--checkInternet()

      function checkLocationEnabled() {
        utils.putMsg.info("Ejcutando checkLocationEnabled()");
        diagnosticSvc.isLocationEnabled()
        .then(function(enabled){
          if(!enabled){
            if(ionic.Platform.isAndroid()){
              utils.showConfirmDialog(
                "La geolocalización no está habilitada, ¿desea habilitarla ahora?"
              ).then(function(){
                diagnosticSvc.switchToLocationSettings();
                helperEnableLocation();
              }, function () {
                utils.showAlertDialog(
                  "La aplicación requiere de la geolocalización para funcionar correctamente."+
                  "<br>Una vez que decida activarla deberá reiniciar la aplicación."
                ).then(function () {
                  checkSession();
                });
              });
            }
            else{//Es iOS
              utils.putMsg.warn("La geolocalización no está habilitada.");
              utils.showAlertDialog(
                "La aplicación necesita que habilite los servicios de geolocalización"
              ).then(function () {
                checkSession();
              });
            }
          }
          else{//Está habilitada!
            fetchInitialLocation();
          }
        }, function(error){
          utils.showAlertDialog(
            "No se pudo determinar el estado del GPS ya que: "+
            (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
          ).then(function () {
            checkSession();
          });
        })
      }//<--checkLocationEnabled()

      var contEnabledLoc = 0;
      function helperEnableLocation() {
        utils.putMsg.info("Ejcutando helperEnableLocation()");
        if(contEnabledLoc==6){//1/2 minuto
          contEnabledLoc = 0;
          checkLocationEnabled();
          return;
        }

        utils.timeout(function () {
          diagnosticSvc.isLocationEnabled()
            .then(function (enabled) {
              if(enabled){
                fetchInitialLocation();
                contEnabledLoc=0;
              }
              else{
                helperEnableLocation();
                contEnabledLoc++;
              }
            }, function (error) {
              utils.showAlertDialog(
                "No se pudo determinar el estado del GPS ya que: "+
                (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
              ).then(function () {
                checkSession();
              });
            })
        }, 5*ONE_SECOND);
      }//<--helperEnableLocation()

      function fetchInitialLocation(){
        utils.putMsg.info("Ejcutando fetchInitialLocation()");
        gpsLocationSvc.init()
        .then(function(resp){
          utils.putMsg.success("Geolocalización Inicial terminada!");
          checkSession();
        }, function(error){
          utils.showAlertDialog(
            "No se pudo iniciar la Geolocalización ya que: "+
            (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
          ).then(function () {
            checkSession();
          });
        });
      }//<--fetchInitialLocation()

      function checkSession() {
        utils.putMsg.info("Ejcutando checkSession()");
        if(!$rootScope.SESSION_ATTRS.isAuthenticated()){
          utils.showConfirmDialog(
            "¿No has iniciado sesión, deseas iniciar ahora?"
          ).then(function () {
            $rootScope.goToState("sesion");
          }, function () {
            //:(
          }).finally(function () {
            checkBGMode();
          });
        }
        else
          checkBGMode();
      }//<--checkSession()


      //TODO: Check for plugins required (must be already installed)
      //...

      //utils.speech("Bienvenido");


      function checkBGMode() {
        //Todo: verificar si solo aplica para android
        utils.putMsg.info("Ejcutando checkBGMode()");
        if($rootScope.MODULES.BG_MODE){

          cordova.plugins.backgroundMode.setDefaults({
            title: "Botón Bluetooth",
            text: 'En alerta'
          });

          // Enable background mode
          cordova.plugins.backgroundMode.enable();

          cordova.plugins.backgroundMode.onfailure = function(errorCode) {
            utils.showAlertDialog(
              "No se puede ejecutar esta aplicación en segundo plano, code: "+errorCode
            );
          };

          cordova.plugins.backgroundMode.onactivate = function () {
            utils.putMsg.info("App en segundo plano.");
          };

        }
      }//<--checkBGMode()


      utils.timeout(function () {
        document.addEventListener("online", function(){
          //$rootScope.isOnline = true;
          utils.showToast("Internet <span style='color: lightgreen;'>Conectado</span>");
        }, false);//ON-ONLINE

        document.addEventListener("offline", function(){
          //$rootScope.isOnline = false;
          utils.showToast(
            "Internet <b style='color:red'>Desconectado</b>"
          );
        }, false);//ON-OFFLINE
      },5*ONE_SECOND);

    },false); //<--Deviceready
  });//<--PlatformReady
});

"use strict";
appControllers
.controller("alertasCtrl", function($rootScope, $scope, $sce, utils){

  $scope.$on('$ionicView.enter', function(e) {
    if(!$rootScope.MODULES.ALERTS)return;
  });

  utils.putMsg.info("-- alertasCtrl");


  $scope.alertas = [];

  //Meto una alerta para que haya algo que comparar si es la 1a vez
  //que se leen de la BD.
  $scope.alertas.push({
    "titulo":"Sin avisos",
    "fecha": utils.fecha().hoy + " / "+ utils.fecha().horas + ":" + utils.fecha().minutos+ ":" + utils.fecha().segundos,
    "mensaje": "En esta sección podrás ver avisos importantes.",
    lat:"",
    lng:""
  });
  //para debugueo...
  $scope.contRequestAlertas = 0;

  function getAlertContent(){
    //utils.putMsg.info("Buscando alertas...");
    $scope.contRequestAlertas++;
    var alertas = [];

    utils.myHttpGet(API_URL + "public/alertas")
    .then(function(API){
      var showAlert = false;

      if(API.result){
        API.data.forEach(function(b){
          alertas.push({
            "titulo":  $sce.trustAsHtml(b.titulo),
            "fecha":   b.fecha,
            "mensaje": $sce.trustAsHtml(b.mensaje)
          });
        });
      }
      else{
        utils.putMsg.log("(ERROR) getAlertContent(): "+API.error_message);
      }


      //Si hay alertas...
      if(alertas.length){
        //Si hay menos alertas que antes... actualizo mi arreglo
        if(alertas.length < $scope.alertas.length){
          showAlert      = true;
          $scope.alertas = alertas;
          //localNotification();
        }else{
          //Es el mismo numero de alertas, pero pudo haberse actualizado el contenido
          //en la BD debio actualizarse tambien su fecha/hora
          if(alertas.length === $scope.alertas.length){
            for(var i=0; i<alertas.length;i++){
              //Si su fecha/hora cambió, la actualizo
              if(alertas[i].fecha   !== $scope.alertas[i].fecha){
                showAlert = true;
                $scope.alertas[i] = alertas[i];
                //localNotification();
              }
            }
          }
          else{//Hay mas alertas que antes!... las meto al arreglo
            showAlert = true;
            //localNotification();
            for(var j=0; j<alertas.length;j++){
              if(j<$scope.alertas.length){
                //otra vez, verifico si es una alerta actualizada
                if(alertas[j].fecha !== $scope.alertas[j].fecha){
                  $scope.alertas[j] = alertas[j];
                }
              }//A partir de aqui van las nuevas alertas!
              else{$scope.alertas.push(alertas[j]);}
            }
          }
        }
      }//<-- Hay alerta?

      if(showAlert){
        utils.timeout(function(){
          $scope.openDrawer();
        }, 2000);}
    },function(error){
      utils.putMsg.log("(ERROR) getAlertContent(): "+error.fullError);
    }).finally(function () {
      utils.timeout(function(){getAlertContent();}, ALERT_TIMER);
    });
  }//<--getAlertContent()

  getAlertContent();

  /**
   * Para cerrar el drawer desde cualquier pagina
   */
  setTimeout(function () {
    if(APP_MODULES.ALERTS && document.querySelector("#my-drawer")){
      //putMsg.info("Cargando drawer");
      $rootScope.closeDrawer = function(){
        //putMsg.log("Cerrando drawer");
        var styleStr = "height: calc(100% - "+(!$rootScope.PLATFORM.isAndroid() ?'64':'44')+"px);"+
          "top:  "+(!$rootScope.PLATFORM.isAndroid() ?'64':'44')+"px;"+
          "transform: translate3d(100%, 0px, 0px);";
        document.querySelector("#my-drawer").setAttribute("style",styleStr);
        //putMsg.log(styleStr);
      };
    }
  }, 800);

});

"use strict";

appControllers.controller('solicitudConsultaCtrl', function($rootScope, $scope, utils){
  utils.putMsg.info("--serviciosConsultaCtrl");

  $scope.casoFldOk = false;
  $scope.statusOk  = false;

  $scope.validateField = function(ticket){

    if(typeof ticket === 'undefined' || ticket === null) ticket = "";

    ticket = ticket.toString();

    $scope.statusOk = false;
    $scope.statusCasoResult = "";
    $scope.casoFldOk = (ticket.length > 0) && (ticket.search(/[^0-9]/) <= 0);
  };

  $scope.statusCasoResult = "Estamos verificando el estado, espere...";
  $scope.verStatus = function(numCaso){
    utils.showLoading();

    $scope.statusOk = false;

    if(numCaso.toString().search(/[^0-9]/) > 0 ){
      utils.showAlertDialog(
        "El número de solicitud <b>" + numCaso + "</b> no es válido, verifique nuevamente."
      );
      utils.hideLoading();
      return;
    }

    utils.myHttpGet(API_URL + "common/denuncia/"+numCaso+"/estatus")
    .then(function (API) {
      if(API.result){
        API = API.data;
        $scope.statusCasoResult = API.nombre;
        $scope.statusOk = true;
      }
      else{
        var e = utils.getError(API, "verStatus(API)");
        utils.showAlertDialog(
          "No se pudo obtener el estatus de la solicitud ya que: "+
          (DEBUG_MODE?'<hr>'+e.fullError:e.reason)
        );
      }
    },
    function (error) {
      var e = utils.getError(error, "verStatus()");
      utils.showAlertDialog(
        "No se pudó obtener el estatus de la solicitud <b>"+ numCaso +"</b>"+' ya que: '+
        (DEBUG_MODE ? '<hr>'+e.fullError : e.reason)+
        ", intente nuevamente más tarde."
      );
    }).finally(function () {
      utils.hideLoading();
    });


  };

  $scope.goToRegisterForm = function () {
    $rootScope.goToState('sesion');
  }
});

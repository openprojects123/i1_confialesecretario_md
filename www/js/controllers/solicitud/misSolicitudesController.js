"use strict";

appControllers.controller('misSolicitudesCtrl', function($rootScope, $scope, utils){
  utils.putMsg.info("--misSolicitudesCtrl");

  $scope.$on('$ionicView.enter', function(e) {
    $scope.denunciasList = [];
    getDenuncias();
  });


  function getDenuncias(){
    utils.showLoading();
    utils.myHttpGet(
      API_URL + "public/solicitudes/usuario/"+ $rootScope.SESSION_ATTRS.getUserId()
    ).then(function (API) {
        if(API.result){
          localStorage.misDenuncias = JSON.stringify(API.data);

          var denuncias = API.data;
          denuncias.forEach(function(data){
            $scope.denunciasList.push({
              numero: data.idticket,
              tipo:   data.nombre,
              fecha: data.fechaabrio
            });
          });
        }
        else{
          var e = utils.getError(API, "getDenuncias(API)");
          utils.showAlertDialog(
            "No se pudo determinar si existen solicitudes ya que: "+
            (DEBUG_MODE?'<hr>'+e.fullError:e.reason)
          );
        }
      },
      function (error) {
        var e = utils.getError(error, "getDenuncias()");
        utils.showAlertDialog(
          "No se pudo determinar si existen solicitudes ya que: "+
          (DEBUG_MODE?'<hr>'+e.fullError:e.reason)
        )
      }).finally(function () {
      utils.hideLoading();
    });


  }//<--getDenuncias()

  $scope.reverse        = false;
  $scope.sortField      = 'numero';
  $scope.numSortClass   = "ion-arrow-up-b";
  $scope.tipoSortClass  = "";
  $scope.fechaSortClass = "";

  $scope.sortByNum = function(){
    $scope.sortField ='numero';
    $scope.reverse = !$scope.reverse;
    $scope.numSortClass = ($scope.reverse) ? "ion-arrow-down-b" : "ion-arrow-up-b";
    $scope.tipoSortClass  = "";
    $scope.fechaSortClass = "";
  };

  $scope.sortByTipo = function(){
    $scope.sortField ='tipo';
    $scope.reverse = !$scope.reverse;
    $scope.tipoSortClass  = ($scope.reverse) ? "ion-arrow-down-b" : "ion-arrow-up-b";
    $scope.numSortClass =  "";
    $scope.fechaSortClass = "";
  };

  $scope.sortByFecha = function(){
    $scope.sortField ='fecha';
    $scope.reverse = !$scope.reverse;
    $scope.fechaSortClass  = ($scope.reverse) ? "ion-arrow-down-b" : "ion-arrow-up-b";
    $scope.numSortClass =  "";
    $scope.tipoSortClass  = "";
  };

  $scope.showStatus = function(numCaso){
    utils.showLoading();

    utils.myHttpGet(API_URL + "estatus/solicitud/" + numCaso)
      .then(function(API){
        if(API.result){
          API = API.data;
          utils.putMsg.info("El estatus se obtuvo correctamente: "+ API.nombre);
          utils.showAlertDialog(
            "El estatus de la solicitud <b>"+ numCaso +"</b> es: <b>"+API.nombre+"</b>",
            {title: "Estatus"}
          );
        }
        else{
          var e = utils.getError(API, "showStatus(API)");
          utils.showAlertDialog(
            "No se pudo obtener el estatus de la solicitud <b>"+ numCaso +"</b>"+' ya que: '+
            (DEBUG_MODE?'<hr>'+e.fullError:e.reason)
          );
        }

      }, function(error){
        var e = utils.getError(error, "showStatus()");
        utils.showAlertDialog(
          "No se pudo obtener el estatus de la solicitud <b>"+ numCaso +"</b>"+' ya que: '+
          (DEBUG_MODE ? '<hr>'+e.fullError:e.reason)+
          ", intente nuevamente más tarde."
        );
      }).finally(function(){
      utils.hideLoading();
    });
  };
});

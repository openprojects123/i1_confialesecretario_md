"use strict";
appControllers
.controller("solicitudFormCtrl",
function($rootScope, $scope, $location, $mdDialog, $ionicScrollDelegate, $state,
         solicitudesFormSvc, webLocationSvc, mapSvc, utils){

  utils.putMsg.info("-- solicitudFormCtrl");


  var
    mapLoaded      = false,
    initLocationOk = false;//ya se realizó la localizacion inicial?

  $scope.catEstados      = [];
  $scope.catMunicipios   = [];
  $scope.catCategoria    = [];
  $scope.catSubcategoria = [];

  /* INICIALIZACION DE CAMPOS PARA EL ENVIO DEL FORMULARIO*/
  $scope.solicitudForm = {
    categoria:   {},
    subcategoria:{},
    fecha:       {selected:null, min:null, max:null},
    estado:      {},
    municipio:   {},
    colonia:     "",
    calle:       "",
    numero:      "",
    referencias: "",
    relatoHechos:"",
    lat:         null,
    lng:         null
  };

  var hoyDate = utils.fecha().full;
  $scope.solicitudForm.fecha.selected = hoyDate;
  $scope.solicitudForm.fecha.min = new Date(
    hoyDate.getFullYear()-1,
    hoyDate.getMonth(),
    hoyDate.getDate()
  );

  $scope.solicitudForm.fecha.max = hoyDate;



  /*************************************
   *         CARGA DE CATALOGOS        *
   *************************************/

  utils.hideLoading();
  utils.showLoading();

  //OBTENER LISTA DE HECHOS
  solicitudesFormSvc.getCatCategoria(1)//<-- iddenuncia
  .then(function(hechos){
    $scope.catCategoria            = hechos;
    $scope.solicitudForm.categoria = hechos[0];

    $scope.getCatSubcategorias(hechos[0].idhechos);
  }, function(error){
    utils.hideLoading();
  });

  //OBTENER LISTA DE DELITOS
  $scope.getCatSubcategorias = function(idHechos){
    if($scope.catSubcategoria)
      utils.showLoading();

    solicitudesFormSvc.getCatSubcategorias(idHechos)
    .then(function(delitos){
      $scope.catSubcategoria            = delitos;
      $scope.solicitudForm.subcategoria = delitos[0];

      if(!$scope.catEstados.length)
        getCatEstados();
      else
        utils.hideLoading();
    },
    function(error){
      utils.hideLoading();
    });
  };


  function getCatEstados() {
    solicitudesFormSvc.getCatEstados()
      .then(function(estados){
        $scope.catEstados           = estados;
        $scope.solicitudForm.estado = estados[0];
        $scope.getCatMunicipios(estados[0].id);
      }, function(error){
        utils.hideLoading();
      });
  }


  $scope.getCatMunicipios = function(idEstado){
    utils.showLoading();

    solicitudesFormSvc.getCatMunicipios(idEstado)
      .then(function(municipios){
        $scope.catMunicipios           = municipios;
        $scope.solicitudForm.municipio = municipios[0];

        if(!initLocationOk)
          getInitialLocation();
        else
          fillDireccion();
      }, function(error){
        utils.hideLoading();
      });
  };

  $scope.municipiosChg = function(){
    fillDireccion();
  };

  /*************************************
   *       FIN CARGA DE CATALOGOS      *
   *************************************/



  /****************************************************************
  //            Obtencion de localizacion inicial                 *
  /****************************************************************/
  function getInitialLocation() {
    webLocationSvc.getLocationOnce()
    .then(function(pos){
      utils.hideLoading();
      utils.putMsg.success("hechosPosition: " + JSON.stringify(pos));
      $rootScope.devicePosition = pos;
      mapLoaded = true;
      fillDireccion();
    }, function(error){
      utils.hideLoading();
      utils.showAlertDialog(
        "No fue posible utilizar la geolocalización, deberá reiniciar la aplicación"+
        " para poder enviar su solicitud de forma correcta."+
        (DEBUG_MODE?"<hr>webLocationSvc->getInitialLocation(): "+error.fullError:'')
      );
    });
  }
  /****************************************************************
  //            Fin Obtencion de localizacion inicial             *
  /****************************************************************/


  //CONFIRMACION PARA ENVIAR FORMULARIO
  $scope.confirmSubmit = function(){
    utils.showConfirmDialog(
      "¿Desea enviar ahora la solicitud?<br> Si los datos son correctos presione "+
      "<b>Aceptar</b>, si no está seguro y desea corregirlos, presione <b>Cancelar</b>"
    ).then(function(){
      denunciaSubmit();
    });
  };

  //ENVIO DEL FORMULARIO
  function denunciaSubmit() {
    utils.showLoading();

    var formData = {
      idHecho:       $scope.solicitudForm.categoria.idhechos,
      idDelito:      $scope.solicitudForm.subcategoria.iddelitos,
      fechaHora:     $scope.solicitudForm.fecha.selected,
      idEstado:      $scope.solicitudForm.estado.id,
      idMunicipio:   $scope.solicitudForm.municipio.id,
      colonia:       $scope.solicitudForm.colonia,
      calle:         $scope.solicitudForm.calle,
      numero:        $scope.solicitudForm.numero       || "N/A",
      entreCalles:   $scope.solicitudForm.referencias  || "N/A",
      descripcion:   $scope.solicitudForm.relatoHechos || "Sin descripcion",
      lat:           $rootScope.hechosPosition.lat,
      lng:           $rootScope.hechosPosition.lng
    };

    //fixme: remover testing
    /*utils.putMsg.log(formData);
    utils.hideLoading();
    return;*/

    solicitudesFormSvc.saveDenuncia(formData)
    .then(function(resp){
      utils.putMsg.log("idTicket Generado: " + resp.idTicket);
      saveRelacionTicketUsuario(resp.idTicket);
    }, function(error){
      utils.hideLoading();
      utils.showAlertDialog(
        "No se pudo enviar el formulario ya que: "+
        (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
      );
    });
  }//Submit()

  function saveRelacionTicketUsuario(idTicket){
    solicitudesFormSvc.saveRelacionTicketUsuario(idTicket)
    .then(function(resp){
      utils.putMsg.success("saveRelacionTicketUsuario: " + JSON.stringify(resp));
    },function(error){

    }).finally(function () {
      //todo:inserto datos de la denuncia en archivo local
      /*solicitudesFormSvc.insertLocalRecord({
        numero: idTicket,
        tipo:   $scope.solicitudForm.categoria.title,
        fecha:  utils.fecha().hoy + utils.fecha().currtime
      });*/

      $scope.showSurvey({
        idhechos:    $scope.solicitudForm.categoria.idhechos,
        iddelitos:   $scope.solicitudForm.subcategoria.iddelitos,
        idTicket:    idTicket
      });

      clearDenunciaForm();
      //utils.hideLoading();
    });
  }


  //envio un evento para abrir el form de surveys
  //Activada desde BOTON si esta en debug, de lo contrario: func interna,
  $scope.showSurvey = function(denunciaObj){
    utils.showAlertDialog(
      "A continuación se le presentará una serie de cuestionarios.<br>"+
      "Para terminar el proceso deberá contestar las secciones necesarias y presionar"+
      "el botón <b>Enviar</b>",
      {
        title:   "Guardar Solicitud",
        ok:  "Entendido"
      }
    ).then(function(){
      utils.putMsg.info("Mostrando Survey");
      $rootScope.goToState('solicitudSurvey', {denuncia: denunciaObj});
    });
  };

  function clearDenunciaForm(){
    //Todo: setPristine this form
    //utils.putMsg.log("Limpiando formulario");
    //Limpio el formulario...
    $scope.solicitudForm.fecha.selected = utils.fecha().full;
    $scope.solicitudForm.colonia        = "";
    $scope.solicitudForm.calle          = "";
    $scope.solicitudForm.numero         = "";
    $scope.solicitudForm.referencias    = "";
    $scope.solicitudForm.relatoHechos   = "";
    fillDireccion();
  }


  function fillDireccion(pos, dir){
    if(dir){
      $rootScope.hechosPosition    = pos;
      $scope.solicitudForm.colonia = dir.colonia;
      $scope.solicitudForm.calle   = dir.calle;
      $scope.solicitudForm.numero  = dir.calleNumero;
      return;
    }

    utils.showLoading();
    webLocationSvc.reverseCoding({
      nomEstado:    $scope.solicitudForm.estado.title,
      nomMunicipio: $scope.solicitudForm.municipio.title,
      colonia:      "", // || $scope.solicitudForm.colonia,
      calle:        "" //$scope.solicitudForm.calle
    }).then(function(location){
      $rootScope.hechosPosition = location;

      webLocationSvc.getFullAddress(location)
        .then(function(direccion){
          //utils.putMsg.log(direccion);
          $scope.solicitudForm.colonia = direccion.colonia;
          $scope.solicitudForm.calle   = direccion.calle;
          $scope.solicitudForm.numero  = direccion.calleNumero;
        }, function(error){
          utils.showAlertDialog(
            "No se pudo obtener la dirección ya que: "+
            (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
          );
        });
    }, function(error){
      utils.showAlertDialog(
        "No se pudo localizar la dirección debido a: "+
        (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
      );
    }).finally(function () {
      utils.hideLoading();
    });

  }

  function placeMarker(pos){
    utils.showLoading();

    /*if(pos.lat && pos.lng){
      $rootScope.hechosPosition = pos;
    }*/
    utils.putMsg.info("Nueva ubicacion: " + JSON.stringify(pos));
    webLocationSvc.getFullAddress(pos)
    .then(function(dir){
      utils.hideLoading();

      fillDireccion(pos, dir);
      mapSvc.setCenter(pos);
      mapSvc.placeMarker(pos);
      mapSvc.placeInfoWindow(webLocationSvc.addressString());

    },function(error){
      utils.hideLoading();
      utils.showAlertDialog(
        "No se pudo obtener la dirección ya que: "+
        (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
      );
    });
  }

  function localizame(){
    utils.showLoading();
    webLocationSvc.getLocationOnce()
      .then(function(pos){
        //utils.putMsg.log(pos);
        utils.putMsg.success("Localizame: " + JSON.stringify(pos));
        //$rootScope.devicePosition = pos;//<--nunca la utilizo :(
        $rootScope.hechosPosition = pos;

        webLocationSvc.getFullAddress(pos)
          .then(function(dir){
            //utils.putMsg.log(dir);
            if(dir.estado.toLowerCase() !== $scope.solicitudForm.estado.title.toLowerCase() ||
              dir.municipio.toLowerCase() !== $scope.solicitudForm.municipio.title.toLowerCase())
            {
              utils.showAlertDialog(
                "El estado y/o municipio seleccionado "+
                "no corresponde con su ubicación actual, verifique.<br>"+
                "<b>Estado</b><br>seleccionado: "+$scope.solicitudForm.estado.title+"<br>"+
                "Actual: "+dir.estado+"<br><b>Municipio</b><br>"+
                "Seleccionado: "+$scope.solicitudForm.municipio.title+"<br>"+
                "Actual: "+dir.municipio
              );
              //$scope.closeMapModal();
              return;
            }

            fillDireccion(pos, dir);
            webLocationSvc.placeMarker(pos);
            webLocationSvc.placeInfoWindow();
            //if(DEBUG_MODE)mapSvc.drawPath();
          },function(error){
            utils.showAlertDialog(
              "No se pudo obtener la dirección debido a que: "+
              (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
            );
          });
      }, function(error){
        utils.showAlertDialog(
          "*No se pudo localizar la dirección debido a que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
        );
      }).finally(function () {
        utils.hideLoading();
      });
  }

  $scope.openMapModal = function () {
    $mdDialog.show({
      controller: modalController,
      templateUrl: 'templates/solicitudes/map-modal.html',
      parent: angular.element(document.querySelector("ion-pane")),
      //targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: true // Only for -xs, -sm breakpoints.
    })
    .then(function() {
      //utils.putMsg.log("Dialogo cerrado");
    });
  };

  function modalController($scope, $mdDialog) {
    utils.timeout(function () {
      mapSvc.init("map1", $rootScope.hechosPosition)
        .then(function (_) {
          mapSvc.placeMarker(mapSvc.getCenter());
          mapSvc.placeInfoWindow(
            webLocationSvc.addressString() || "Dirección desconocida"
          );
          mapSvc.setClickMapListener(placeMarker);
        }, function (error) {
          utils.showAlertDialog(
            "El mapa no se cargó debido a que: "+
            (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
          )
        });
    }, 800);

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.locateme = function() {
      localizame();
      //$mdDialog.hide();
    };
  }

  $scope.showMapHelp = function(){
    utils.showAlertDialog(
      "Pruebe haciendo clic en algún lugar del mapa para autocompletar "+
      "los campos de la dirección.<br>"+
      "<b>Nota</b>: la dirección obtenida es aproximada.",
      {
        title: "Instrucciones",
        ok: "Entendido"
      }
    );
  };

});

"use strict";
appControllers
.controller('solicitudSurveyCtrl',
function($rootScope, $scope, $state, $ionicScrollDelegate, $ionicNavBarDelegate,
         surveyPreguntasSvc,surveyVehiculosSvc, surveySujetosSvc, surveyObjetosSvc,
         surveyMultimediaSvc, utils){

  utils.putMsg.info("--solicitudSurveyCtrl");

  $scope.$on("$ionicView.enter", function(event, data){
    $ionicNavBarDelegate.showBackButton(false);
    //utils.putMsg.info("Ocultando botones del header.")
  });

  $scope.solicitud = $state.params.denuncia;
  //utils.putMsg.log($scope.solicitud)
  if((!$scope.solicitud || typeof $scope.solicitud === 'undefined') && DEBUG_MODE){
    utils.putMsg.error("No se recibio el objeto denuncia, se creara uno.");
    $scope.solicitud = {
      idhechos:  3,
      iddelitos: 2,
      idTicket:  192
    };
  }

  utils.putMsg.info($scope.solicitud);


  $scope.preguntas            = {items:[]}; //La respuesta la insertaré aqui tambien (ver el ngRepeat)
  $scope.numRespuestas        = 0;

  $scope.vehiculos            = {marcas: null, modelos: null, colores: null};
  $scope.vehiculoFormData     = {marcaSel:null, modeloSel:null, colorSel:{}, anio:'', placas:'', descripcion:''};
  $scope.vehiculosToSaveList  = [];

  $scope.sujetoGenerales = {
    nombre: "",
    alias:  "",
    acento: "",
    voz:    "",
    rasgo:  ""
  };
  $scope.sujetoCaracteristicas = [];
  $scope.sujetosToSaveList     = [];

  $scope.objetos          = {categorias:null, subcategorias:null};
  $scope.objetoFormData   = {categoriaSel:null, subCategoriaSel:null, cantidad:'', descripcion:''};
  $scope.objetoToSaveList = [];

  var idComentaTicket;
  $scope.multimedia    = {comentarios:''};

  //Lista de formularios que se muestran en el Menu
  $scope.formularios =[
    {name: "Preguntas"},
    {name: "Vehículos"},
    {name: "Sujetos"},
    {name: "Objetos"},
    {name: "Multimedia"}
  ];

  $scope.botonesFAB = {
    vehiculos:  false,
    sujetos:    false,
    objetos:    false,
    multimedia: true
  };

  $scope.afterLoadErrorsList = [];//[{recursoName, errorObj}, ...]
  $scope.afterSaveErrorsList = [];
  $scope.numBadUploadedFiles = 0;

  /*/$scope.showMultimediaTooltips  = false;
  function dimishTooltips(){
    utils.timeout(function () {
      $scope.showMultimediaTooltips = false;
    }, 2500);
  }*/
  $scope.formShowed = "Menu";
  $scope.showForm = function(formName){
    //utils.putMsg.log("Mostrando formulario: "+formName);
    $scope.formShowed = formName;

    if(formName === "Menu"){
      $ionicScrollDelegate.scrollTop();
    }

    $scope.botonesFAB.multimedia = formName === 'Multimedia';
    if(formName === 'Multimedia'){
      /*utils.timeout(function () {
        $scope.showMultimediaTooltips  = true;
        dimishTooltips();
      }, 200);*/
    }

    utils.timeout(function () {
      $scope.$apply();
    }, 400);
  };

  utils.showLoading();

  //------------------------- METODOS FORM PREGUNTAS -----------------------
  surveyPreguntasSvc.getPreguntas(
    1, $scope.solicitud.idhechos, $scope.solicitud.iddelitos
  ).then(function (_preguntas) {
    $scope.preguntas = _preguntas;
  }, function (error) {
    $scope.afterLoadErrorsList.push({recursoName: "Preguntas", errorObj: error})
  }).finally(function () {
    loadVehiculosMarcas();
  });

  //Para saber cuantas preguntas se han contestado
  $scope.respuestaBlur = function(respuesta){
    $scope.numRespuestas = surveyPreguntasSvc.getNumRespuestas($scope.preguntas.items);
  };
  //*********************** FIN METODOS FORM PREGUNTAS *********************



  //------------------------- METODOS FORM VEHICULOS -----------------------
  function loadVehiculosMarcas() {
    surveyVehiculosSvc.getMarcas()
    .then(function (_marcas) {
      $scope.vehiculos.marcas = _marcas;
      $scope.vehiculoFormData.marcaSel = _marcas[0];
      loadVehiculosModelos(_marcas[0].id);
      loadVehiculosColores();
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Marcas de vehículos", errorObj: error})
    }).finally(function () {
      loadSujetos();
    });
  }

  $scope.onMarcaChange = function(idMarca) {
    loadVehiculosModelos(idMarca, true);
  };

  function loadVehiculosModelos(idmarcavehiculo, byChange) {//byChange = fired by onChangeMarca?
    if(byChange === true)utils.showLoading();

    surveyVehiculosSvc.getModelos(idmarcavehiculo)
    .then(function (_modelos) {
      $scope.vehiculos.modelos = _modelos;
      $scope.vehiculoFormData.modeloSel = _modelos[0];
      $scope.toggleVehiculosButton();
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Modelos de vehículos", errorObj: error})
    }).finally(function () {
      if(byChange){
        utils.hideLoading();
        checkIfGotLoadedErrors();
      }
    });
  }

  function loadVehiculosColores() {
    surveyVehiculosSvc.getColores()
    .then(function (_colores) {
      $scope.vehiculos.colores = _colores;
      $scope.vehiculoFormData.colorSel = _colores[0];
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Colores de vehículos", errorObj: error})
    });
  }

  $scope.toggleVehiculosButton = function(){
    utils.timeout(function () {
      $scope.botonesFAB.vehiculos =
        ($scope.vehiculoFormData.marcaSel.title !== "N/A" ||
        $scope.vehiculoFormData.modeloSel.title !== "N/A" ||
        //$scope.vehiculo.anio.length > 0 ||
        $scope.vehiculoFormData.placas.length > 0 ||
        $scope.vehiculoFormData.descripcion.length == 6)
    }, 200);
  };

  $scope.addVehiculo = function(){
    if(surveyVehiculosSvc.addVehiculo($scope.vehiculoFormData, $scope.vehiculosToSaveList)){
      //Ahora limpio los campos
      $scope.vehiculoFormData.marcaSel       = $scope.vehiculos.marcas[0];
      loadVehiculosModelos($scope.vehiculos.marcas[0].id);
      $scope.vehiculoFormData.colorSel       = $scope.vehiculos.colores[0];
      $scope.vehiculoFormData.anio           = "";
      $scope.vehiculoFormData.placas         = "";
      $scope.vehiculoFormData.descripcion    = "";
      $scope.botonesFAB.vehiculos  = false;
      utils.timeout(function () {
        $ionicScrollDelegate.scrollBottom();
        utils.showToast("El vehículo ha sido agregado");
      }, 100);
    }
    else{
      utils.showAlertDialog("No se pudo agregar el vehículo");
    }
  };

  $scope.deleteVehiculo = function(idx){
    $scope.vehiculosToSaveList.splice(idx, 1);
    utils.timeout(function () {
      $ionicScrollDelegate.scrollTop();
    }, 100);
    utils.showToast("El vehículo ha sido removido");
  };
  //*********************** FIN METODOS FORM VEHICULOS *********************



  //------------------------- METODOS FORM SUJETOS -----------------------
  function loadSujetos() {
    surveySujetosSvc.getSujetos()
    .then(function (_catalogos) {
      $scope.sujetoCaracteristicas = _catalogos;
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Características sujetos", errorObj: error})
    }).finally(function () {
      loadObjetosCategorias();
    })
  }

  $scope.toggleSujetosButton = function(){
    utils.timeout(function () {
      var enable = false;
      $scope.sujetoCaracteristicas.some(function(cat){
        if(cat.selectedOption.title !== "N/A"){
          //$scope.showMultimediaTooltips = true;
          //dimishTooltips();
          return enable = true;
        }
      });
      $scope.botonesFAB.sujetos = enable;
    },10);
  };

  $scope.addSujetos = function(){
    if(
      surveySujetosSvc.addSujeto(
        [$scope.sujetoGenerales, $scope.sujetoCaracteristicas],
        $scope.sujetosToSaveList
      )
    ){
      //Ahora limpio los campos...
      $scope.sujetoGenerales.nombre = "";
      $scope.sujetoGenerales.alias  = "";
      $scope.sujetoGenerales.acento = "";
      $scope.sujetoGenerales.voz    = "";
      $scope.sujetoGenerales.rasgo  = "";

      utils.timeout(function () {
        $ionicScrollDelegate.scrollBottom();
      }, 100);

      $scope.botonesFAB.sujetos = false;
      utils.showToast("El sujeto ha sido agregado");
    }
    else{
      utils.showAlertDialog("No se pudo agregar el sujeto");
    }
  };

  $scope.deleteSujeto = function(idx){
    $scope.sujetosToSaveList.splice(idx, 1);
    utils.timeout(function () {
      $ionicScrollDelegate.scrollTop();
    }, 100);
    utils.showToast("El sujeto ha sido removido");
  };
  //*********************** FIN METODOS FORM SUJETOS *********************



  //------------------------- METODOS FORM OBJETOS -----------------------
  function loadObjetosCategorias() {
    surveyObjetosSvc.getObjetosCategorias()
    .then(function (_categorias) {
      $scope.objetos.categorias = _categorias;
      $scope.objetoFormData.categoriaSel = _categorias[0];
      loadObjSubcategorias(_categorias[0].id);
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Categoría de objetos", errorObj: error})
      utils.hideLoading();
      checkIfGotLoadedErrors();
    });
  }

  $scope.onObjCatChange = function (objetoId) {
    loadObjSubcategorias(objetoId);
    $scope.toggleObjetosButton();
  };

  function loadObjSubcategorias(idCategoria, byObjCatChange) {//byChange = fired by onObjCatChange?
    if(byObjCatChange === true)utils.showLoading();

    surveyObjetosSvc.getObjetosSubcategorias(idCategoria)
    .then(function (_subcategorias) {
      $scope.objetos.subcategorias = _subcategorias;
      $scope.objetoFormData.subCategoriaSel = _subcategorias[0];
    }, function (error) {
      $scope.afterLoadErrorsList.push({recursoName: "Subcategoría de objetos", errorObj: error})
    }).finally(function () {
      utils.hideLoading();
      checkIfGotLoadedErrors();
    })
  }

  $scope.toggleObjetosButton = function(){
    $scope.botonesFAB.objetos = (
      $scope.objetoFormData.categoriaSel.title !== "N/A"
    );
    /*utils.timeout(function () {
      $scope.$apply();
      //$scope.showMultimediaTooltips = $scope.botonesFAB.objetos;
      //dimishTooltips();
    }, 300);*/
  };

  $scope.addObjetos = function(){
    if(surveyObjetosSvc.addObjeto($scope.objetoFormData, $scope.objetoToSaveList)){
      //Reset form
      $scope.objetoFormData.categoriaSel   = $scope.objetos.categorias[0];
      loadObjSubcategorias($scope.objetos.categorias[0].id);
      $scope.objetoFormData.cantidad    = "";
      $scope.objetoFormData.descripcion = "";
      $scope.botonesFAB.objetos         = false;
      utils.timeout(function () {
        $ionicScrollDelegate.scrollBottom();
      }, 100);
      utils.showToast("El objeto ha sido agregado");
    }
    else{
      utils.showAlertDialog("El objeto no pudo ser agregado");
    }
  };

  $scope.deleteObjeto = function(idx){
    $scope.objetoToSaveList.splice(idx, 1);
    utils.timeout(function () {
      $ionicScrollDelegate.scrollTop();
    }, 100);
    utils.showToast("El objeto ha sido removido");
  };
  //*********************** FIN METODOS FORM OBJETOS *********************


  //------------------------- METODOS FORM MULTIMEDIA -----------------------
  $scope.confirmDeleteFile = function(id){
    utils.showConfirmDialog(
      "¿Seguro que desea quitar el archivo <b>"+ $rootScope.fileList[id].nombre +
      "</b> de la lista?"
    ).then(function (API) {
      surveyMultimediaSvc.removeFile(id)
      .then(function(fileList){
        //$rootScope.fileList = fileList;
      }, function(error){
        utils.showAlertDialog(
          "No se pudo quitar el archivo ya que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
        );
      });
    });
  };
  //*********************** FIN METODOS FORM MULTIMEDIA *********************

  function checkIfGotLoadedErrors() {
    var totalErrors = $scope.afterLoadErrorsList.length;
    if(totalErrors){
      var msg = totalErrors>1?
          "No se pudieron cargar los siguientes catálogos:<hr>":
          "No se pudo cargar el catálogo:<br>";

      $scope.afterLoadErrorsList.forEach(function (err) {
        msg+= "<b>"+err.recursoName+"</b> ya que: "+
        (DEBUG_MODE? err.errorObj.fullError:err.errorObj.reason)+"<br>";
      });
      utils.showAlertDialog(msg);
    }
  }

  $scope.verifyAndSend = function(){

    var errForms = findFormsToSave();
    if(errForms.length){
      utils.showAlertDialog(
        "Aun hay información sin guardar en los siguientes formularios:<br>"+
        "<b>"+errForms+"</b>"
      );
    }
    else if(
      surveyPreguntasSvc.isFormEmpty($scope.preguntas) &&
      !$scope.vehiculosToSaveList.length &&
      !$scope.sujetosToSaveList.length &&
      !$scope.objetoToSaveList.length
    ){
      utils.showConfirmDialog(
        "No ha contestado ningun formulario, su solicitud será guardada pero ya no podrá "+
        "agregar esta información, excepto la multimedia.<br>¿Aún desea continuar?"
      ).then(function () {
        submitForms();
      });
    }
    else{
      submitForms();
    }
  };

  /**
   * Busca que formularios deben ser guardados
   */
  function findFormsToSave() {
    var notEmptyForms = "";//Nombre de los forms que aun tienen info sin guardar
    if(surveyVehiculosSvc.hasUnsavedData($scope.vehiculoFormData))
      notEmptyForms += "Vehículos<br>";
    if(surveySujetosSvc.hasUnsavedData(
        [$scope.sujetoGenerales, $scope.sujetoCaracteristicas]))
      notEmptyForms += "Sujetos<br>";
    if(surveyObjetosSvc.hasUnsavedData($scope.objetoFormData))
      notEmptyForms += "Objetos<br>";
    if($rootScope.fileList.length && $scope.multimedia.comentarios.length < 10)
      notEmptyForms += "Multimedia: (debe agregar un comentario de al menos 10 caracteres)<br>";

    return notEmptyForms;
  }

  //Todo hacer algo con los numItemsSaved
  function submitForms(){
    var idTicket = $scope.solicitud.idTicket;
    utils.showLoading();
    surveyPreguntasSvc.savePreguntas(idTicket, $scope.preguntas)
      .then(function (numItemsSaved) {
        if(numItemsSaved == -1){
          utils.putMsg.info("No hay preguntas que guardar.")
        }else{
          utils.putMsg.success(numItemsSaved+" preguntas almacenadas");
          $scope.preguntas = {items:[]};
        }
      }, function (error) {
        $scope.afterSaveErrorsList.push({formName:"Preguntas", error: error});
      }).finally(function () {
      surveyVehiculosSvc.saveVehiculos(idTicket, $scope.vehiculosToSaveList)
        .then(function (numItemsSaved) {
          if(numItemsSaved == -1){
            utils.putMsg.info("No hay vehículos que guardar.")
          }else{
            utils.putMsg.success(numItemsSaved+" vehículos almacenados");
            $scope.vehiculosToSaveList = [];
          }
        }, function (error) {
          $scope.afterSaveErrorsList.push({formName:"Vehículos", error: error});
        })
        .finally(function () {
          surveySujetosSvc.saveSujetos(idTicket, $scope.sujetosToSaveList)
            .then(function (numItemsSaved) {
              if(numItemsSaved == -1){
                utils.putMsg.info("No hay sujetos que guardar.")
              }else{
                utils.putMsg.success(numItemsSaved+" características de sujetos almacenadas");
                $scope.sujetosToSaveList = [];
              }
            }, function (error) {
              $scope.afterSaveErrorsList.push({formName:"Sujetos", error: error});
            }).finally(function () {
              surveyObjetosSvc.saveObjetos(idTicket, $scope.objetoToSaveList)
                .then(function (numItemsSaved) {
                  if(numItemsSaved == -1){
                    utils.putMsg.info("No hay objetos que guardar.")
                  }else{
                    utils.putMsg.success(numItemsSaved+" objetos almacenados");
                    $scope.objetoToSaveList = [];
                  }
                }, function (error) {
                  $scope.afterSaveErrorsList.push({formName:"Objetos", error: error});
                }).finally(function () {
                  updateTicketStatus(idTicket);
                });
            });
        });
    })
  }//<--verifyAndSubmit()

  //Actualizo el estatus de la denuncia a 1
  function updateTicketStatus(idTicket){
    surveyMultimediaSvc.updateTicketStatus(idTicket, 1)
    .then(function(resp){
      utils.putMsg.info("statusTicket actualizado");
    }, function(error){

    }).finally(function () {
      if($scope.multimedia.comentarios.length)
        saveComentaTicket();
      else
        checkIfGotSavedErrors();
    });
  }

  function saveComentaTicket() {

    surveyMultimediaSvc.saveComentaTicket($scope.solicitud.idTicket, $scope.multimedia.comentarios || "*Sin comentarios" )
    .then(function (resp) {
      idComentaTicket = resp.idComentario;
      utils.putMsg.success("idComentario: "+idComentaTicket);
      uploadMultimedia();
    }, function (error) {
      //utils.putMsg.error("surveyMultimediaSvc->saveComentaTicket()");
      checkIfGotSavedErrors();
    });
  }

  function uploadMultimedia() {
    utils.hideLoading();

    var uploadStatePopup;
    if($rootScope.fileList.length){
      //Muestro el popup
      uploadStatePopup = utils.popup({
        title:       "",
        templateUrl: "templates/partials/fileUploadsTableTpl.html",
        cssClass:    "popupUploadsTable",
        buttons:     false
      });
    }
    else{
      checkIfGotSavedErrors();
      return;
    }

    surveyMultimediaSvc.uploadFileList()
    .then(function (numUploadedOk) {
      utils.putMsg.success(numUploadedOk+" Archivos bien subidos");
      if(!numUploadedOk)
        checkIfGotSavedErrors();
      else
        saveMultiComentaTicket();
    }, function (err) {
      //En teoria nunca entra aqui...
      utils.putMsg.error(err.fullError);
    }).finally(function () {
      if(typeof uploadStatePopup.close === 'function'){
        uploadStatePopup.close();
      }
    })
  }

  function saveMultiComentaTicket() {
    var uploadedFileObjList = [];
    $rootScope.fileList.forEach(function (file) {
      if(file.subidoOk){
        uploadedFileObjList.push(file);
      }
    });

    if(!uploadedFileObjList.length){
      checkIfGotSavedErrors();
      return;
    }


    utils.showLoading();
    surveyMultimediaSvc.saveMultiComentaTicket($scope.solicitud.idTicket, idComentaTicket, uploadedFileObjList)
    .then(function (numInserts) {
      utils.putMsg.info("MulticomentaTicket inserts: "+numInserts);
    }, function (error) {
      //utils.putMsg.error("surveyMultimediaSvc->saveMultiComentaTicket(): "+error.fullError);
    }).finally(function () {
      checkIfGotSavedErrors();
    });
  }


  /**
   * Verifica si hubo errores despues de intentar guardar todos los forms
   */
  function checkIfGotSavedErrors() {
    utils.hideLoading();

    var formErrorsStr = "";
    $scope.afterSaveErrorsList.forEach(function (formError) {
      formErrorsStr += "<b>"+formError.formName + "</b>"+' ya que: '+
      (DEBUG_MODE? formError.error.fullError+'<hr>':formError.error.reason+"<br>")
    });

    var uploadErrorsStr = "";
    var uploadedIdxList = [];
    $rootScope.fileList.forEach(function (file, idx) {
      if(!file.subidoOk){
        file.total = 0;
        $scope.numBadUploadedFiles++;
        uploadErrorsStr += "<b>"+ file.nombre+"</b>"+" ya que: "+
        (DEBUG_MODE?file.errorObj.fullError+'<hr>':file.errorObj.reason+"<br>");
      }
      else
        uploadedIdxList.push(idx);//id del archivo subido correctamente
    });

    //Remuevo todos los archivos que sí se subieron
    uploadedIdxList.forEach(function (id) {
      surveyMultimediaSvc.removeFile(id);
    });

    if(formErrorsStr.length){
      utils.showAlertDialog(
        "Los siguientes formularios no se pudieron guardar<hr>"+formErrorsStr
      ).then(function () {
        if($scope.numBadUploadedFiles){
          utils.showConfirmDialog(
            "Los siguientes archivos no se subieron<hr>"+uploadErrorsStr+
            "¿Desea intentar subirlos nuevamente?",
            {
              ok:     "Sí",
              cancel: "No"
            }
          ).then(function () {

          }, function () {
            $rootScope.fileList           = [];
            $scope.multimedia.comentarios = "";
            showFinalMessage();
          });
        }
        else
          showFinalMessage();
      });
      $scope.afterSaveErrorsList = [];
    }
    else if($scope.numBadUploadedFiles){
      utils.showConfirmDialog(
        "Los siguientes archivos no se subieron<hr>"+uploadErrorsStr+
        "¿Desea intentar subirlos nuevamente?",
        {
          ok:     "Sí",
          cancel: "No"
        }
      ).then(function () {

      }, function () {
        $rootScope.fileList           = [];
        $scope.multimedia.comentarios = "";
        showFinalMessage();
      });
    }
    else{
      $rootScope.fileList           = [];
      $scope.multimedia.comentarios = "";
      showFinalMessage();
    }
  }

  function showFinalMessage() {
    utils.showAlertDialog(
      "Su solicitud ha sido recbida, posteriormente puede consultarla su estatus "+
      "con el número: <b>"+$scope.solicitud.idTicket+"</b> o en el apartado "+
      "<em>Mis solicitudes</em> así como también agregar archivos multimedia."
    ).then(function () {
      $rootScope.goHome();
    });
  }

});

"use strict";
appControllers
.controller("solicitudMultimediaCtrl", function($scope, $rootScope, $stateParams, $state,
  solicitudesFormSvc, surveyMultimediaSvc, utils){

  utils.putMsg.info("--solicitudMultimediaCtrl");

  $scope.denunciaTipo     = $stateParams.tipo;
  $scope.catCategoria    = [];
  $scope.catSubcategoria = [];

  $scope.ticketFieldOk    = false;
  $scope.ticketFormOk     = false;

  $scope.numBadUploadedFiles = 0;

  var idComentaTicket;

  /* INICIALIZACION DE CAMPOS PARA EL ENVIO DEL FORMULARIO*/
  $scope.multimediaForm ={
    idSolicitud:  "",
    categoria:    {},
    subcategoria: {},
    IOS_FilePath: ""
  };


  //TODO hacer algo con los errores
  //*************************************
  //          CARGA DE CATALOGOS        *
  //*************************************
  utils.showLoading();

  //OBTENER LISTA DE HECHOS
  solicitudesFormSvc.getCatCategoria(1)//<-- iddenuncia
    .then(function(hechos){
      $scope.catCategoria             = hechos;
      $scope.multimediaForm.categoria = hechos[0];

      $scope.getCatSubcategorias(hechos[0].idhechos);
    }, function(error){
      utils.hideLoading();
    });

  //OBTENER LISTA DE DELITOS
  $scope.getCatSubcategorias = function(idHechos){
    if($scope.catSubcategoria)
      utils.showLoading();

    solicitudesFormSvc.getCatSubcategorias(idHechos)
    .then(function(delitos){
      $scope.catSubcategoria             = delitos;
      $scope.multimediaForm.subcategoria = delitos[0];

        utils.hideLoading();
    },
    function(error){
      utils.hideLoading();
    });
  };

  /*************************************
   *       FIN CARGA DE CATALOGOS      *
   *************************************/

  $scope.validateField = function (ticket){
    if(typeof ticket === 'undefined' || ticket === null){
      ticket = "";
    }
    ticket = ticket.toString();

    $scope.ticketFieldOk = false;
    $scope.ticketFormOk  = false;

    $scope.ticketFieldOk = (ticket.length > 0) && (ticket.search(/[^0-9]/) <= 0);
  };

  $scope.validateTicket = function () {
    utils.showLoading();

    var dataForm = {
      idticket:   $scope.multimediaForm.idSolicitud,
      iddenuncia: 1,
      idhecho:    $scope.multimediaForm.categoria.idhechos,
      iddelito:   $scope.multimediaForm.subcategoria.iddelitos
    };

    solicitudesFormSvc.validateTicket(dataForm)
    .then(function(resp){
      if(resp.encontrado === 1 || resp.encontrado === "1"){
        $scope.ticketFormOk  = true;
      }
      else{
        utils.showAlertDialog(
          "El número de solicitud <b>" + dataForm.idticket +"</b> no es válidado, "+
          "verifique que coincida el tipo de solicitud así como el tipo de hecho."
        );
      }
    },
    function(error){
      utils.showAlertDialog(
        "Ocurrió un error al intentar validar la solicitud ya que: "+
        (DEBUG_MODE? '<hr>'+error.fullError : error.reason)+
        ", intentelo nuevamente más tarde."
      );
    }).finally(function () {
      utils.hideLoading();
    });
  };//Validar Ticket

  $scope.clearForm = function(){
    var totFiles = $rootScope.fileList.length;
    utils.showConfirmDialog(
      "El comentario "+
      (totFiles ? (totFiles>1 ? "y los "+totFiles+" archivos ":" y el archivo ") :'')+
      "se eliminarán del formulario, ¿desea continuar?"
    ).then(function () {
      $scope.ticketFormOk    = false;
      $scope.ticketFieldOk   = false;
      $rootScope.fileList    = [];
      $scope.multimediaForm.idSolicitud = "";
      $scope.multimediaForm.comentarios = "";
    });
  };


  $scope.confirmDeleteFile = function(id){
    utils.showConfirmDialog(
      "¿Seguro que desea quitar el archivo <b>"+ $rootScope.fileList[id].nombre +
      "</b> de la lista?"
    ).then(function () {
      surveyMultimediaSvc.removeFile(id)
      .then(function(fileList){
        //$rootScope.fileList = fileList;
      }, function(error){
        utils.showAlertDialog(
          "No se pudo quitar el achivo ya que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
        );
      });
    });
  };

  //BOTON: Subir multimedia
  $scope.validateAndSubmit = function(){
    var
      totFiles = $rootScope.fileList.length,
      content  = "";

    if(!totFiles){
      content = "No ha agregado archivos, ¿desea enviar sólo los comentarios?";
    }
    else{
      content =  "¿Desea subir ahora ";
      content += (totFiles>1 ? "los " + totFiles + " archivos" : " el archivo");
      content += " y el comentario?";
    }

    utils.showConfirmDialog(content)
    .then(function () {
      saveComentaTicket();
    });
  };//<-- confirmMultimediaSubmit()

  function saveComentaTicket() {
    surveyMultimediaSvc.saveComentaTicket($scope.multimediaForm.idSolicitud,$scope.multimediaForm.comentarios || "*Sin comentarios")
      .then(function (resp) {
        idComentaTicket = resp.idComentario;
        utils.putMsg.success("idComentario: "+idComentaTicket);
        uploadMultimedia();
      }, function (error) {
        checkIfGotSavedErrors();
      });
  }

  function uploadMultimedia(){
    var uploadStatePopup = utils.popup({
      title:       "",
      templateUrl: "templates/partials/fileUploadsTableTpl.html",
      cssClass:    "popupUploadsTable",
      buttons:     false
    });

    surveyMultimediaSvc.uploadFileList()
      .then(function (numUploadedOk) {
        utils.putMsg.success(numUploadedOk+" Archivos bien subidos");
        if(!numUploadedOk)
          checkIfGotSavedErrors();
        else
          saveMultiComentaTicket();
      }, function (err) {
        //En teoria nunca entra aqui...
        utils.putMsg.error(err.fullError);
      }).finally(function () {
      if(typeof uploadStatePopup.close === 'function'){
        uploadStatePopup.close();
      }
    });
  }

  function saveMultiComentaTicket() {
    var uploadedFileObjList = [];
    $rootScope.fileList.forEach(function (file) {
      if(file.subidoOk){
        uploadedFileObjList.push(file);
      }
    });

    if(!uploadedFileObjList.length){
      checkIfGotSavedErrors();
      return;
    }

    utils.showLoading();
    surveyMultimediaSvc.saveMultiComentaTicket($scope.multimediaForm.idSolicitud, idComentaTicket, uploadedFileObjList)
      .then(function (numInserts) {
        utils.putMsg.info("surveyMultimediaSvc->saveMultiComentaTicket(): "+numInserts);
      }, function (error) {

      }).finally(function () {
      checkIfGotSavedErrors();
    });
  }

  function checkIfGotSavedErrors() {
    //utils.putMsg.log("Checando errores");
    utils.hideLoading();

    var uploadErrorsStr = "";
    var uploadedIdxList = [];
    $rootScope.fileList.forEach(function (file, idx) {
      if(!file.subidoOk){
        file.total = 0;
        $scope.numBadUploadedFiles++;
        uploadErrorsStr += "<b>"+ file.nombre+"</b>"+" ya que: "+
        (DEBUG_MODE?file.errorObj.fullError+'<hr>':file.errorObj.reason+"<br>");
      }
      else
        uploadedIdxList.push(idx);//id del archivo subido correctamente
    });

    //Remuevo todos los archivos que sí se subieron
    uploadedIdxList.forEach(function (id) {
      surveyMultimediaSvc.removeFile(id);
    });

    if($scope.numBadUploadedFiles){
      utils.showConfirmDialog(
        "Los siguientes archivos no se subieron<hr>"+uploadErrorsStr+"<br>"+
        "¿Desea inter subirlos nuevamente?",
        {
          ok:     "Sí",
          cancel: "No"
        }
      ).then(function () {

      }, function () {
        $rootScope.fileList               = [];
        $scope.multimediaForm.idSolicitud = "";
        $scope.multimediaForm.comentarios = "";
        showFinalMessage();
      });
    }
    else{
      $rootScope.fileList               = [];
      $scope.multimediaForm.idSolicitud = "";
      $scope.multimediaForm.comentarios = "";
      showFinalMessage();
    }
  }

  function showFinalMessage() {
    //utils.showAlertDialog("");
  }

});

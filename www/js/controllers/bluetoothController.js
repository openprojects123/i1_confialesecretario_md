"use strict";

appControllers

.controller("bluetoothCtrl",
function($rootScope, $scope, botonBLESvc, emergencySvc, userSvc, utils){
  utils.putMsg.info("--bluetoothCtrl");

  $scope.$on('$ionicView.enter', function(e) {
    $scope.user = userSvc.getUser();

    botonBLESvc.isEnabled()
    .then(function(resp){
      $scope.isBLEEnabled = resp;
      utils.putMsg.info("isBLEEnabled: "+ $scope.isBLEEnabled);
    }, function(error){
      $scope.isBLEEnabled = false;
      if(error !== false)
        utils.showAlertDialog(
          "No fue posible determinar el estado del Bluetooth ya que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
        );
    });
  });


  $scope.platform = $rootScope.PLATFORM.platform();

  $scope.enableBLE = function(){
    botonBLESvc.enable()
    .then(function(resp){
      $scope.isBLEEnabled = resp;
    }, function(error){
      $scope.isBLEEnabled = false;
      utils.showAlertDialog(
        "No se pudo activar el Bluetooth ya que: "+
        (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
      );
    });
  };

  function connectBLE(successCallback, failCallback){
    //utils.putMsg.log("Verificando que el BLE esté habilitado...");

    utils.showLoading();

    botonBLESvc.isEnabled()
    .then(function(){
      //utils.putMsg.log("isEnabled():true. Iniciando escaneo del BLE");
      botonBLESvc.startScan($scope.user.buttonMAC)
      .then(function(data){
        utils.putMsg.success("Botón encontrado!");
        connectBT(data.id, successCallback, failCallback);
      }, function(error){
        utils.showAlertDialog(
          "No se encontró el botón Bluetooth ya que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)+
          "<br>Asegúrese de encenderlo e intente vincularlo nuevamente."
        );
        utils.hideLoading();
      });
    }, function(error){
      $scope.isBLEEnabled = false;

      if(error !== false){
        utils.showAlertDialog(
          "No fue posible determinar el estado del Bluetooth ya que: "+
          (DEBUG_MODE?"<hr>"+error.fullError:error.reason)
        );
        return;
      }

      if($rootScope.PLATFORM.isAndroid())
        utils.showConfirmDialog(
          "El bluetooth no está activado, ¿Desea activarlo ahora?"
        ).then(function(){
          enableBT();
        });
    }).finally(function () {
      utils.hideLoading();
    });

    var enableBT = function(){
      utils.showLoading();
      botonBLESvc.enable()
      .then(function(){
        connectBLE();
      }, function(error){
        utils.showAlertDialog(
          "No se pudo habilitar el bluetooth, ya que: "+
          (DEBUG_MODE?'<hr>'+error.fullError:error.reason)+
          " deberá habilitarlo manualmente."
        );
      }).finally(function () {
        utils.hideLoading();
      });
    };

    var connectBT = function(id, onBLEClicked, onBLEDisconnect){
      utils.putMsg.info("Intentando conectar con el BLE: "+id);
      botonBLESvc.connect(id, onBLEClicked, onBLEDisconnect)
      .then(function(device){
        bleButton.innerHTML = "Desvincular Botón Bluetooth";
        if(APP_MODULES.BG_MODE){
          cordova.plugins.backgroundMode.setDefaults({
            title: "Botón Bluetooth",
            text: 'En alerta'
          });
        }
      },function(error){
        utils.showAlertDialog(
          "No se pudo conectar con el Botón ya que: "+
          (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
        );
      });
    };
  }

  $scope.toggleBtn = function(){
    if(!$rootScope.SESSION_ATTRS.isAuthenticated()){
      $rootScope.goToState("sesion");
      return;
    }

    if($rootScope.IS_BTN_CONNECTED === false){
      connectBLE(onBLEClicked, onBLEDisconnect);
    }
    else if($rootScope.IS_BTN_CONNECTED === true){
      //utils.putMsg.info("Desconectando el botón");
      botonBLESvc.disconnect().then(
      function(){
        bleButton.innerHTML = "Vincular Botón Bluetooth";
      },
      function(error){
        utils.showAlertDialog(
          "No se pudo desconectar el botón ya que: "+
          (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
        );
      });
    }
    else{
      utils.showAlertDialog("El botón se encuentra en un estado inconsistente");
      utils.hideLoading();
    }
  };

  function onBLEClicked(){
    if($rootScope.IS_TRACKING){
      emergencySvc.stopEmergencyAndTracking();
      return;
    }
    emergencySvc.startEmergencyAndTracking();
  }

  function onBLEDisconnect() {
    $rootScope.IS_BTN_CONNECTED = false;

    navigator.vibrate([400, 300, 400, 400, 800]);

    cordova.plugins.backgroundMode.setDefaults({
      title: "Advertencia",
      text: 'El botón se ha desconectado'
    });

    utils.showAlertDialog("El botón se ha desconectado");
  }
});

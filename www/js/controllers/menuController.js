"use strict";

appControllers.controller('menuLateralCtrl',
  function($scope, $rootScope, $state, $ionicHistory, utils){

    $rootScope.menuTabs = [
      {
        name: 'Inicio',
        url:  'inicio',
        class: '',
        icon: 'icon-left ion-home'
      },{
        name: 'Servicios',
        class: '',
        items: [
          {title: 'Realizar solicitud', url: '#/solicitudForm'},
          {title: 'Consultar solicitud', url: '#/solicitudConsulta'}
        ]
      },{
        name: 'Contáctanos',
        url:  'contactanos',
        class: '',
        icon: 'ion-information-circled',
        items: []
      }
    ];

    if(APP_MODULES.SESSION.USER_AUTH) {
      $rootScope.menuTabs.splice(2, 0,
        {
          name: 'Mi cuenta',
          url: "",
          class: "",
          icon: "",
          items: [
            {title: 'Mi sesión', url: '#/sesion'},
            {title: 'Mi perfil', url: '#/perfil'}
          ]
        }
      );

      //Espero a que se inicialicen las cosas del servicio userSvc
      //incluido dentro de diagnosticsController
      utils.timeout(function () {
        if($rootScope.SESSION_ATTRS.isAuthenticated()){
          $rootScope.menuTabs[1].items.splice(2, 0,
            {title: 'Mis solicitudes', url: '#/misSolicitudes'}
          );
        }
      }, ONE_SECOND);



      $rootScope.$on("session-started", function (event, data) {
        if($rootScope.menuTabs[1].items.url != "#/misSolicitudes")
          $rootScope.menuTabs[1].items.splice(2, 0,
            {title: 'Ver mis solicitudes', url: '#/misSolicitudes'}
          );
      });

      $rootScope.$on("session-finished", function (event, data) {
        if($rootScope.menuTabs[1].items.url === "#/misSolicitudes")
          $rootScope.menuTabs[1].items.splice(2, 1);
      })

    }

    if($rootScope.MODULES.BLUETOOTH){
      $rootScope.menuTabs.splice($rootScope.menuTabs.length-1, 0, {
        name: 'Botón Bluetooth',
        url:  'bluetooth',
        class: '',
        icon: 'ion-bluetooth',
        items: []
      });
    }

    $scope.activeGroup = $rootScope.menuTabs[0];

    $scope.toggleGroup = function (group) {
      //alert("toggle");
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.isActiveGroup= function (group) {
      return $scope.activeGroup === group;
    };

    $rootScope.activateGroup = function(menuTab){
      $scope.activeGroup = menuTab;
    };

    $scope.isActiveChild = function (url) {
      return url === "#"+$state.current.url;
    };

    $scope.goToTab = function(tab){
      if(tab.url !== $state.current.name){
        //utils.putMsg.info("llendo a: "+tab.url);
        $ionicHistory.currentView($ionicHistory.backView());
        $state.go(tab.url, {}, {location: 'replace'});
      }
      $scope.activeGroup = tab;
    };

    $rootScope.goToState = function(state, params){
      /*if(params && DEBUG_MODE)
        console.info("params:", params || "ninguno");*/

      $state.go(state, params);
    };

    $rootScope.goHome = function(){
      $ionicHistory.clearHistory();
      $state.go('inicio', {}, {location: 'replace'});
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });
      $rootScope.activateGroup($rootScope.menuTabs[0]);
    };

  });

"use strict";

appControllers.controller('sesionCtrl', function($rootScope, $scope, $mdDialog, userSvc, utils){
  $scope.loginForm = {};
  $scope.loginForm.isRemembered = true;

  $scope.user = userSvc.getUser();
  //utils.putMsg.log($scope.user);

  $scope.doLogin = function(form){
    utils.showLoading();
    userSvc.login($scope.loginForm)
      .then(function () {
        $scope.user = userSvc.getUser();

        utils.putMsg.success("Login userId: "+$rootScope.SESSION_ATTRS.getUserId());

        $scope.loginForm.loginField    = "";
        $scope.loginForm.passwordField = "";
        $scope.loginForm.isRemembered  = true;

        form.$setPristine();
        form.$setUntouched();

        utils.showToast(
          "Bienvenido <span style='color: aqua'>"+$scope.user.nombre+"</span>"
        );

        $rootScope.$broadcast("session-started");

        $rootScope.goHome();

      }, function (error) {
        utils.showAlertDialog(
          "No se pudo iniciar sesión  ya que: "+
          (DEBUG_MODE ? '<hr>'+error.fullError: error.reason)+
          ", inténtelo más tarde."
        )
      }).finally(function () {
      utils.hideLoading();
    })
  };

  $scope.doLogout = function () {
    utils.showConfirmDialog(
      "¿Realmente desea cerrar la sesión?"
    ).then(function () {
      userSvc.logout();
      $rootScope.$broadcast("session-finished");
    })
  };

  $scope.changePasswordForm = {};
  $scope.passwordChangeForm = function () {
    $mdDialog.show({
      controller: modalController,
      templateUrl: 'templates/sesion/modal-changePassword.html',
      parent: angular.element(document.body),
      //targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: false // Only for -xs, -sm breakpoints.
    }).then(function(form) {
      utils.showLoading();
      userSvc.changePassword(form)
        .then(function (resp) {
          utils.putMsg.log("Contraseña actualizada!");
          utils.showToast("Su contraseña ha sido actualizada", 4*ONE_SECOND);
        }, function (error) {
          utils.showAlertDialog(
            "No se pudo actualizar la contraseña ya que: "+
            (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
          );
        }).finally(function () {
        utils.hideLoading();
      });
    });
  };

  $scope.recoveryPasswordForm = {};
  $scope.passwordRecoveryForm = function () {
    $mdDialog.show({
      controller: modalController,
      templateUrl: 'templates/sesion/modal-passwordRecovery.html',
      parent: angular.element(document.body),
      //targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: false // Only for -xs, -sm breakpoints.
    })
      .then(function(form) {
        utils.showLoading();
        userSvc.recoveryPassword(form)
          .then(function (info) {
            utils.putMsg.log("Contraseña recuperada!");
            utils.showAlertDialog(
              "La contraseña ha sido enviada a la cuenta de correo (<b>"+info+"</b>)"
            );
          }, function (error) {
            utils.showAlertDialog(
              "No se pudo actualizar la contraseña debido a que: "+
              (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
            );
          }).finally(function () {
          utils.hideLoading();
        });
      });
  };

  $scope.newUserForm = {};
  $scope.userRegitrationForm = function () {
    $mdDialog.show({
      controller: modalController,
      templateUrl: 'templates/sesion/modal-userRegistration.html',
      parent: angular.element(document.body),
      //targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: true // Only for -xs, -sm breakpoints.
    })
      .then(function(form) {
        utils.showLoading();
        userSvc.register(form)
          .then(function () {
            utils.putMsg.log("Usuario registrado!");
            utils.showConfirmDialog(
              "Su cuenta ha sido creada y hemos enviado un <b>código</b> de confirmación al correo: <b>"+form.email+"</b><br>"+
              "Para activar la cuenta debe introducir dicho código. ¿Desea introducirlo ahora?"
            ).then(function () {
              $scope.codeValidationForm();
            });
          }, function (error) {
            utils.showAlertDialog(
              "No se pudo completar el registro ya que: "+
              (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
            )
          }).finally(function () {
          utils.hideLoading();
        });
      });
  };

  $scope.validateCodeForm = {};
  $scope.codeValidationForm = function () {
    $mdDialog.show({
      controller: modalController,
      templateUrl: 'templates/sesion/modal-userCodeValidation.html',
      parent: angular.element(document.body),
      //targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false // Only for -xs, -sm breakpoints.
    }).then(function(form) {
        utils.showLoading();
        userSvc.validateCode(form.codigo)
        .then(function () {
          utils.putMsg.log("Codigo validado!");
          $scope.user = userSvc.getUser();
          $rootScope.$broadcast("session-started");
          $rootScope.goHome();
        }, function (error) {
          utils.showAlertDialog(
            "No se pudo validar  el código ya que: "+
            (DEBUG_MODE?'<hr>'+error.fullError:error.reason)
          )
        }).finally(function () {
          utils.hideLoading();
        });
      });
  };

  function modalController($scope, $mdDialog) {
    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.doRegister = function() {
      $mdDialog.hide($scope.newUserForm);
    };

    $scope.doRecoveryPassword = function () {
      $mdDialog.hide($scope.recoveryPasswordForm);
    };

    $scope.doChangePassword = function () {
      $mdDialog.hide($scope.changePasswordForm);
    };

    $scope.validateCode = function () {
      $mdDialog.hide($scope.validateCodeForm);
    }
  }

  })

.controller("perfilCtrl", function($scope, userSvc){
  $scope.user = userSvc.getUser();

  //console.log($scope.user);


});

#!/usr/bin/env node

//this hook installs all your plugins

// add your plugins to this list--either
// the identifier, the filesystem location
// or the URL
var pluginlist = [
  "cordova-plugin-geolocation",
  "cordova-plugin-file-transfer",
  "cordova-plugin-inappbrowser",
  "cordova-plugin-network-information",
  "cordova-plugin-background-mode",
  "cordova.plugins.diagnostic",
  "https://github.com/KingAndrew1024/cordova-plugin-ble-central.git",
  //"cordova-plugin-x-toast",
  "cordova-plugin-tts",
  "cordova-plugin-vibration",
];

var pluginsAndroid = [
  //"https://github.com/MaginSoft/MFileChooser.git",      //<--Android
  "https://github.com/KingAndrew1024/bgGeolocation.git" //<--Android
];

var pluginsIOS = [
  "cordova-background-geolocation-lt"
];


var currPlatform = null;
var rootdir = process.argv[2];
if (rootdir) {

  // go through each of the platform directories that have been prepared
  var platforms = (process.env.CORDOVA_PLATFORMS ? process.env.CORDOVA_PLATFORMS.split(',') : []);

  for(var x=0; x<platforms.length; x++) {
    // open up the index.html file at the www root
    try {
      var platform = platforms[x].trim().toLowerCase();
      currPlatform = platform;
    } catch(e) {
      process.stdout.write(e);
    }
  }

}

// no need to configure below

var fs = require('fs');
var path = require('path');
var sys = require('util')
var exec = require('child_process').exec;

function puts(error, stdout, stderr) {
    console.log(stdout)
}

console.log("currPlatform: "+currPlatform);
if(currPlatform !== null && currPlatform === "android"){
  pluginsAndroid.forEach(function(p_android){
    pluginlist.push(p_android);
    console.log("Agregando android plugin: "+p_android);
  });
}
else if(currPlatform !== null && currPlatform === "ios"){
  pluginsIOS.forEach(function(p_iOS){
    pluginlist.push(p_iOS);
    console.log("Agregando iOS plugin: "+p_iOS);
  });
}



var command = (currPlatform !== null && currPlatform === "ios") ? "sudo " : "";
pluginlist.forEach(function(plug) {
  console.log("Agregando plugin: "+plug);
    exec(command + "cordova plugin add " + plug, puts);
});
